'use strict';

const EventEmitter = require('events');
const uuid = require('uuid').v4;
const shared = require("./shared");
const NodeRDKafka = require('node-rdkafka');

class NodeRdKafkaProducer extends EventEmitter {

    brokers;
    clientId;
    isConnected;
    log;
    configuration;
    moduleName;

    #producerIdleDisconnectMs;
    #nodeRDKafkaConfig = null;
    #nodeRDKafkaTopicConfig = null;
    #producer = null;
    #pollInterval;
    #batchSize;
    #lowater;

    #maxTokensInFlight;
    #nextToken = null;
    #tokensInFlight = null;
    #inFlightResolves = null;
    #clientTimeout = null;
    #ackResolves = null;



    constructor(brokers, configuration, logger, parentName = null) {
        super();
        if (parentName) this.moduleName = parentName + "/" + "NodeRdKafkaProducer";
        else this.moduleName = "NodeRdKafkaProducer";

        if (!logger?.info && logger.log) logger = logger.log;
        if (!(logger?.error && logger?.warn && logger?.info && logger?.debug)) {
            this.log = console;
            this.log.info("Custom logger does not support log.error, log.warn and/or log.info. Reverting to console");
        }
        else if (logger?.child) this.log = logger.child({ module: this.moduleName, context: brokers.join(",") });
        else this.log = logger;

        this.configuration = configuration ?? {};

        this.brokers = shared.toArray(brokers);
        if (!brokers.length) shared.throwError(this.log, "Brokers must be spesified in AdminClient constructor");

        this.clientId = this.configuration?.clientId || uuid();

        this.isConnected = false;
        this.#maxTokensInFlight = this.configuration.procucerMaxMessagesInFlight || 200;
        this.#pollInterval = this.configuration.pollInterval || 10;
        this.#producerIdleDisconnectMs = this.configuration.producerIdleDisconnectMs ?? 20000;


        this.#nextToken = 0;
        this.#tokensInFlight = {};
        this.#ackResolves = {};

        this.#batchSize = Math.round(this.#maxTokensInFlight * 0.2);
        if (this.#batchSize < 1) this.#batchSize = 1;

        this.#lowater = Math.round(this.#maxTokensInFlight * 0.2) + 1;
        if (this.#lowater < 1) this.#lowater = 1;

        this.#inFlightResolves = [];

        this.#nodeRDKafkaConfig = {
            'client.id': this.clientId,
            'message.max.bytes': this.configuration.messageMaxBytes || 1000000000,
            'metadata.broker.list': this.brokers,
            'max.in.flight.requests.per.connection': this.configuration.maxInFlightRequestsPerConnection || 1,
            'retry.backoff.ms': this.configuration.retryBackoffMs || 10,
            'message.send.max.retries': this.configuration.messageSendMaxRetries || 10,
            'socket.keepalive.enable': this.configuration.socketKeepaliveEnable || true,
            'queue.buffering.max.messages': this.configuration.queueBufferingMaxMessages || this.#maxTokensInFlight * 2,
            'linger.ms': this.configuration.lingerMs || 100,
            'batch.num.messages': this.configuration.batchNumMessages || this.#batchSize,
            'dr_cb': true
        };
        this.#nodeRDKafkaTopicConfig = {
            'request.required.acks': this.configuration.requestRequiredAcks || 1,
            //'compression.codec': this.#producerConfig.compressionCodec
        }
        this.log.info("Initialized");
    }

    async send(item = {}, awaitConfirmation = false) {
        /*if (this.#clientTimeout)
            clearTimeout(this.#clientTimeout);*/

        if (!item.topic) shared.throwError(this.log, "record.topic must be provided to use send(): " + this.brokers.toString())
        const topic = item.topic;
        const partition = item.partition || null;
        const record = item.record || "";
        const key = item.key || null;
        let timestamp = null;;
        if (item.timestamp) timestamp = Number(item.timestamp);
        if (isNaN(timestamp)) timestamp = null;



        const token = this.#nextToken;
        this.#nextToken++;

        this.#tokensInFlight[token] = true;
        if (Object.keys(this.#tokensInFlight).length > this.#maxTokensInFlight) {
            //if (!this.#inFlightResolves.length) console.log("Producer paused");
            await new Promise((resolve, reject) => {
                this.#inFlightResolves.push(resolve);
            })
        }
        await this.#connect();
        this.#producer.produce(topic, partition, Buffer.from(record), key, timestamp, token);
        if (awaitConfirmation) {
            await new Promise((resolve, reject) => {
                this.#ackResolves[token] = resolve;
            })
        }
        /*this.#clientTimeout = setTimeout(() => {
            this.log.info("Idle timeout");
            this.emit("idleTimeout", this);
            this.disconnect();
        }, this.#producerIdleDisconnectMs);*/
        return token;
    }

    async tombstone(record, awaitConfirmation) {
        record.message = null;
        return await this.send(record, awaitConfirmation)
    }


    async flush() {
        if (this.isConnected) {
            await new Promise((resolve, reject) => {
                this.#producer.flush(10000, err => {
                    if (err) reject(err);
                    resolve();
                });
            });
        }
    }


    async disconnect() {
        if (this.#clientTimeout)
            clearTimeout(this.#clientTimeout);
        if (this.isConnected) {
            this.isConnected = false;
            await this.flush();
            await new Promise((resolve, reject) => {
                this.#producer.disconnect((err) => {
                    if (err) reject(ett);
                });
                this.#producer.once('disconnected', () => { resolve() });
            });

            this.#producer = null;
            this.emit("disconnected", this);
            this.log.info("Disconnected");
        }
    }

    #handleDeliveryReport(err, report) {
        if (err) shared.throwError(this.log, err);
        const self = this;
        if (this.#tokensInFlight[report.opaque]) {

            delete this.#tokensInFlight[report.opaque];

            if (this.#ackResolves[report.opaque]) {
                this.#ackResolves[report.opaque]();
                delete this.#ackResolves[report.opaque];
            }

            const inFlight = Object.keys(this.#tokensInFlight).length
            if (this.#inFlightResolves.length && inFlight <= this.#lowater) {
                while (this.#inFlightResolves.length) {
                    const resolve = this.#inFlightResolves.shift();
                    if (this.#inFlightResolves.length) resolve(false);
                    else resolve(true);
                }
                this.#inFlightResolves.forEach((resolve) => resolve());
                this.#inFlightResolves = [];
            }
            this.emit("delivered", report.opaque);
        } else {
            shared.throwError(this.log, "Token missmatch: " + this.brokers.toString());
        }
    }

    async #connect() {
        if (this.#clientTimeout) clearTimeout(this.#clientTimeout);

        if (!this.isConnected) {

            this.isConnected = true;
            this.#producer = new NodeRDKafka.Producer(this.#nodeRDKafkaConfig, this.#nodeRDKafkaTopicConfig);

            await new Promise((resolve, reject) => {
                this.#producer.connect((err) => {
                    if (err) reject();
                });

                this.#producer.once('ready', () => { resolve() });
            });
            this.isConnected = true;
            this.#producer.setPollInterval(this.#pollInterval);

            const self = this;
            this.#producer.on('delivery-report', function (err, report) {
                self.#handleDeliveryReport(err, report);
            });
            this.emit("connected", this);
            this.log.info("Connected");

        }

        if (this.#producerIdleDisconnectMs)
            this.#clientTimeout = setTimeout(() => {
                this.log.info("Idle timeout");
                this.emit("idleTimeout", this);
                this.disconnect();
            }, this.#producerIdleDisconnectMs);


    }

}

module.exports = NodeRdKafkaProducer;