'use strict';

const EventEmitter = require('events');
const { Kafka, ConfigResourceTypes } = require('kafkajs');
const uuid = require('uuid').v4;
const shared = require("./shared");

class KafkaJsAdminClient extends EventEmitter {

    /* From AdminClient
    clientId;
    brokers;
    adminClientIdleDisconnectMs;
    isConnected;
    log;
    */
    clientId;
    brokers;
    adminClientIdleDisconnectMs;
    isConnected;
    log;

    #idleTimeout;
    #kafkaJsConnectionConfig;
    #client

    constructor(brokers, configuration, logger, parentName = null) {
        super();

        if (parentName) this.moduleName = parentName + "/" + "KafkaJsAdminClient";
        else this.moduleName = "KafkaJsAdminClient";

        if (!logger?.info && logger.log) logger = logger.log;
        if (!(logger?.error && logger?.warn && logger?.info && logger?.debug)) {
            this.log = console;
            this.log.info("Custom logger does not support log.error, log.warn and/or log.info. Reverting to console");
        }
        else if (logger?.child) this.log = logger.child({ module: this.moduleName, context: brokers.join(",") });
        else this.log = logger;

        this.configuration = configuration ?? {};

        this.brokers = shared.toArray(brokers);
        if (!brokers.length) shared.throwError(this.log, "Brokers must be spesified in AdminClient constructor");

        this.clientId = this.configuration?.clientId || uuid();
        this.adminClientIdleDisconnectMs = this.configuration.adminClientIdleDisconnectMs ?? 20000; //Default is 60 seconds

        this.isConnected = false;
        this.#idleTimeout = null;
        this.#kafkaJsConnectionConfig = {
            clientId: this.clientId,
            brokers: this.brokers
        }
        this.log.info("Initialized");
    }

    async disconnect() {
        if (this.isConnected) {
            
            this.#clearIdleTimeout();
            await this.#client.disconnect();
            this.isConnected=false;
            this.emit("disconnect", this);
            this.log.info("Disconnected");
        }
    }

    async listTopics() {
        await this.#connect();
        return await this.#client.listTopics();
    }

    async

    async getTopicConfig(topic, includeSynonyms = false) {
        if (!topic) shared.throwError(this.log, "Topic must be specified when calling getTopicConfig: " + this.brokers.toString());
        await this.#connect();

        //Get topics on broker
        const topics = await this.listTopics();


        //Get topic description if topic exists
        let description = null;
        if (topics.includes(topic))
            description = await this.#client.describeConfigs({
                includeSynonyms: includeSynonyms,
                resources: [
                    {
                        type: ConfigResourceTypes.TOPIC,
                        name: topic
                    }
                ]
            })

        let result = null;
        if (description) {
            let descriptionRecord = null;
            if (description) {
                descriptionRecord = description.resources[0].configEntries;
            }
            result = {};
            descriptionRecord.forEach(config => {
                const parameter = shared.dotToCamelCase(config.configName);
                result[parameter] = config.configValue;
            })
            const metadata = await this.getTopicMetadata(topic)
            if (metadata) {
                result.replicas = metadata.topics[0].partitions[0].replicas.length;
                result.partitions = metadata.topics[0].partitions.length;
            }
        }
        return result;
    }

    async getValidTopicConfigParameters() {
        await this.#connect();
        const validTopicConfigurationAttributes = [];
        const topics = await this.listTopics()
        if (topics.length) {
            const sampleTopicConfig = await this.#client.describeConfigs({
                includeSynonyms: false,
                resources: [
                    {
                        type: ConfigResourceTypes.TOPIC,
                        name: topics[0]
                    }
                ]
            })
            const configParameters = sampleTopicConfig?.resources[0]?.configEntries;
            if (configParameters) {
                configParameters.forEach((parameter) => {
                    if (!parameter.readOnly) {
                        if (!validTopicConfigurationAttributes.includes(shared.dotToCamelCase(parameter.configName))) validTopicConfigurationAttributes.push(shared.dotToCamelCase(parameter.configName));
                    }
                });
            }
        }
        return validTopicConfigurationAttributes;
    }

    async listBrokers() {
        await this.#connect();
        const brokers = await this.#client.describeCluster();
        return brokers.brokers;
    }

    async getBrokerConfig(nodeId, includeSynonyms = false) {
        if (!nodeId) shared.throwError(this.log, "nodeId must be specified when calling getBrokerConfig: " + this.brokers.toString());
        await this.#connect();
        const result = await this.#client.describeConfigs({
            includeSynonyms: includeSynonyms,
            resources: [
                {
                    type: ConfigResourceTypes.BROKER,
                    name: nodeId.toString()
                }
            ]
        })
        return result?.resources[0]?.configEntries;
    }

    async getTopicMetadata(topic) {
        if (!topic) shared.throwError(this.log, "topic must be specified when calling getTopicOffsets");
        await this.#connect();
        return await this.#client.fetchTopicMetadata({ topics: [topic] });
    }

    async getTopicOffsets(topic) {
        if (!topic) shared.throwError(this.log, "topic must be specified when calling getTopicOffsets");
        await this.#connect();
        return await this.#client.fetchTopicOffsets(topic);
    }

    async deleteTopicRecords(topic, partition, offset) {
        if (!topic) shared.throwError(this.log, "topic must be specified when calling deleteTopicRecords");
        await this.#connect();

        let deleteOffset = null;

        if (!offset) deleteOffset = -1;
        else deleteOffset = offset;

        const partitions = []
        if (partition) {
            partitions.push({ partition: partition, offset: deleteOffset });
        } else {
            const topicOffsets = await this.getTopicOffsets(topic);
            topicOffsets.forEach((topic) => {
                partitions.push({ partition: topic.partition, offset: -1 });
            })
        }

        await this.#client.deleteTopicRecords({
            topic: topic,
            partitions: partitions
        })
        this.log.info("Deleted topic records on broker(s): " + topic + " > " + this.brokers.toString());
    }

    async createTopic(topic, configuration) {
        await this.#connect();

        const brokers = await this.listBrokers();
        let defaultReplicas = brokers.length;
        if (defaultReplicas > 3) defaultReplicas = 3;

        if (!configuration) configuration = {};
        const validTopicConfigurationAttributes = await this.getValidTopicConfigParameters();
        const configEntries = getConfigEntries(configuration, validTopicConfigurationAttributes);

        const result = await this.#client.createTopics({
            waitForLeaders: true,
            topics: [{
                topic: topic,
                numPartitions: configuration.partitions || 1,
                replicationFactor: configuration.replicas || defaultReplicas,
                configEntries: configEntries
            }]
        });
        if (!result)
            shared.throwError(this.log, "Failed to create topic on broker. Topic may exist: " + topic + " > " + this.brokers.toString());
        else
            this.log.info("Created topic on broker(s): " + topic + " > " + this.brokers.toString());

        return result;
    }
    async updateTopic(topic, configuration) {
        await this.#connect();

        const validTopicConfigurationAttributes = await this.getValidTopicConfigParameters();
        const configEntries = getConfigEntries(configuration, validTopicConfigurationAttributes);

        const result = await this.#client.alterConfigs({
            validateOnly: false,
            resources: [{
                type: ConfigResourceTypes.TOPIC,
                name: topic,
                configEntries: configEntries
            }]
        });
        if (!result)
            shared.throwError(this.log, "Failed to update topic config on broker: " + topic + " > " + this.brokers.toString());
        else
            this.log.info("Updated topic config on broker(s): " + topic + " > " + this.brokers.toString());

        return result;
    }

    async deleteTopics(...topics) {
        await this.#connect();

        let result = true;
        try {
            await this.#client.deleteTopics({
                topics: topics,
                //timeout: 10,
            })
        } catch (err) {
            shared.throwError(this.log, "Failed to delete topic on broker(s): " + topic + " > " + this.brokers.toString());
        }
        if (result) this.log.info("Deleted topic on broker(s): " + topic + " > " + this.brokers.toString());
        return result;
    }

    async #connect() {
        this.#clearIdleTimeout();
        if (!this.isConnected) {
            this.isConnected = true
            this.#client = (new Kafka(this.#kafkaJsConnectionConfig)).admin();
            await this.#client.connect();
            this.emit("connected", this);
            this.log.info("Connected");
        }
        this.#setIdleTimeout();
    }

    #setIdleTimeout() {
        if (this.adminClientIdleDisconnectMs) {
            this.#idleTimeout = setTimeout(() => {
                this.log.info("Idle timeout");
                this.emit("idleTimeout", this);
                this.disconnect();
            }, this.adminClientIdleDisconnectMs);
        }
    }

    #clearIdleTimeout() {
        if (this.#idleTimeout) {
            clearTimeout(this.#idleTimeout);
            this.#idleTimeout = null;
        }
    }
}

function getConfigEntries(configuration, validProperties) {
    const configEntries = [];
    Object.keys(configuration).forEach((key) => {
        const dotKey = shared.camelCaseToDotted(key);
        if (validProperties.includes(key))
            configEntries.push({ name: dotKey, value: configuration[key].toString() });
    });

    return configEntries;
}

module.exports = KafkaJsAdminClient;

