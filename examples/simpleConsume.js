'use strict';
const {Topic}= require("../index");

async function main() {
    const testTopic = new Topic("test", "10.10.0.50:9092", {
        consumeMode: 5, //TO_LAST
        //loWater: 100,
        highWater: 1000,
        cleanupPolicy: "compact",
        partitions:1,
        replicas:3,
        //autoCommit: true,
        //commitIntervalMessages: 555,
        //commitIntervalMs: 1000,
        autoUpdateTopicConfig: true,
        maxCompactionLagMs: 70000,
        autoCreateTopic: true,
    }, {
        sessionTimeoutMs: 6000,
        groupId: "test",
        batchSize: 500,
        consumerIdleDisconnectMs: 5000,
        adminClientIdleDisconnectMs: 5000
    });


    testTopic.on("eof", () => {
        console.log("===EOF===");
    })

    const start = new Date();
    let count = 0;
    await testTopic.consume(async (item) => {
        count++;
    }, true)
    const end = new Date();
    console.log("Messages:", count);
    console.log("Time Diff:", ((end - start) / 1000).toFixed(1), "s");
    console.log("Consume speed:", (count / ((end - start) / 1000)).toFixed(0), "msg/s");

    //Disconnect consumer
    await testTopic.consume();
    console.log("DONE");
}

main();