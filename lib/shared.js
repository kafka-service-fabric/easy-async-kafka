'use strict';
const clone = require("clone");
const crypto = require('crypto');
const deepmerge = require("deepmerge");

function hashConfig(config) {
    const sortedObject = sortObject(clone(config));
    const cleanedObject = {};
    Object.keys(sortedObject).map(function (key, index) {
        // Skip "null" values
        if (sortedObject[key] !== undefined && sortedObject[key] !== null && sortedObject[key] !== NaN)
            cleanedObject[key] = sortedObject[key];
    });

    //Crude MD5 hash of JSON
    return crypto.createHash('md5').update(JSON.stringify(cleanedObject)).digest('hex');
}

function sortObject(obj) {
    return Object.keys(obj).sort().reduce(function (result, key) {
        result[key] = obj[key];
        return result;
    }, {});
}

function mergeConfig(defaultConfig, config) {
    if(!defaultConfig) defaultConfig={};
    if(!config) config={};
    const arrayMerge = (destinationArray, sourceArray, options) => sourceArray;
    return deepmerge(defaultConfig, config, { arrayMerge: arrayMerge });
}

function dotToCamelCase(name, firstLetterUpperCase = false) {
    const parts = name.split(".");
    let result = ""
    let isFirst = true;
    parts.forEach(word => {
        if (isFirst) {
            if (firstLetterUpperCase) result = word.substr(0, 1).toUpperCase() + word.substr(1);
            else result = word;
            isFirst = false;
        } else {
            result = result + word.substr(0, 1).toUpperCase() + word.substr(1);
        }
    })
    return result;
}

function camelCaseToDotted(name) {
    return name.split(/(?=[A-Z])/).map(s => s.toLowerCase()).join('.');
}

function toArray(...objects) {
    let arr = objects.flat();
    if (arr[0] === null || arr[0] === undefined) arr = null;
    return arr;
}

function throwError(logger,error){
    logger.error(error);
    throw error;
}


module.exports = {
    hashConfig, mergeConfig, dotToCamelCase, camelCaseToDotted, toArray,throwError
};