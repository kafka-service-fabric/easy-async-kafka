'use strict';

const EventEmitter = require('events');
const async = require('async');
const uuid = require('uuid').v4;
const shared = require("./shared");
const NodeRDKafka = require('node-rdkafka');
const SimpleState = require("./SimpleState");


class NodeRdKafkaConsumer extends EventEmitter {

    #nodeRDKafkaConfig = null;
    #nodeRDKafkaTopicConfig = null;
    #consumer = null;
    #consumerIdleDisconnectMs;
    log = null;
    #topics = null;
    #disconnectTimeout = null;
    #validConsumeModes = [1, 2, 3, 4, 5];
    #commitResolves = {}
    #state = null;

    constructor(brokers, configuration, logger, parentName = null) {
        super();
        if (parentName) this.moduleName = parentName + "/" + "NodeRdKafkaConsumer";
        else this.moduleName = "NodeRdKafkaConsumer";

        if (!logger?.info && logger?.log) logger = logger.log;
        if (!(logger?.error && logger?.warn && logger?.info && logger?.debug)) {
            this.log = console;
            this.log.info("Custom logger does not support log.error, log.warn and/or log.info. Reverting to console");
        }
        else if (logger?.child) this.log = logger.child({ module: this.moduleName, context: brokers.join(",") });
        else this.log = logger;

        this.configuration = configuration ?? {};

        this.brokers = shared.toArray(brokers);
        if (!brokers.length) shared.throwError(this.log, "Brokers must be spesified in AdminClient constructor");

        this.groupId = this.configuration.groupId || uuid();
        this.clientId = this.configuration?.clientId || this.groupId;
        this.#consumerIdleDisconnectMs = this.configuration.consumerIdleDisconnectMs ?? 20000
        this.batchSize = this.configuration.batchSize || 100;

        this.#topics = {};

        this.#nodeRDKafkaConfig = {
            'group.id': this.groupId,
            'metadata.broker.list': this.brokers,
            "socket.keepalive.enable": true,
            "enable.auto.commit": false,
            //"auto.commit.interval.ms": this.#consumerConfiguration.commitIntervalMs,
            "enable.partition.eof": true,
            "heartbeat.interval.ms": this.configuration.heartbeatIntervalMs ?? 3000,
            "session.timeout.ms": this.configuration.sessionTimeoutMs ?? 9000,
            "enable.auto.offset.store": false,
            "fetch.wait.max.ms": this.configuration.maxWaitMs ?? 10,
            //Handle rebalance events on consumer manually
            'rebalance_cb': (err, assignments) => {
                this.#onRebalance(err, assignments)
            },
            'offset_commit_cb': (err, topicPartitions) => {
                if (err) shared.throwError(this.log, err);
                this.#onCommit(topicPartitions);
            }
        }
        this.#nodeRDKafkaTopicConfig = {
            "auto.offset.reset": "beginning",
        }

        this.#state = new SimpleState("DISCONNECTED", {
            "DISCONNECTED": ["DISCONNECTING"],
            "CONNECTING": ["DISCONNECTED"],
            "UNSUBSCRIBED": ["CONNECTING", "UNSUBSCRIBING"],
            "SUBSCRIBING": ["UNSUBSCRIBED"],
            "UNASSIGNED": ["SUBSCRIBING", "ASSIGNED"],
            "ASSIGNED": ["UNASSIGNED"],
            "UNSUBSCRIBING": ["ASSIGNED", "UNASSIGNED"],
            "DISCONNECTING": ["UNSUBSCRIBED"],
        }, this.log);
        this.log.info("Initialized");
    }

    //Check if cunsumer has subscriptions and return result
    hasSubscriptions() {
        if (this.#state.hasState("ASSIGNED") || this.#state.hasState("UNASSIGNED")) return true;
        else return false;
    }

    //Check if consumer is assigned to topic partitions and return result
    isAssigned() {
        if (this.#state.hasState("ASSIGNED")) return true;
        else return false;
    }

    //Check if consumer is connected
    isConnected() {
        if (!this.#state.hasState("DISCONNECTED") && !this.#state.hasState("CONNECTING") && !this.#state.hasState("DISCONNECING")) return true;
        else return false;
    }

    //Check if topic is subscribed and return result
    isTopicSubscribed(topic) {
        if (this.#topics[topic]) return true;
        else return false;
    }

    //Pause consuming for topic
    pause(topic) {
        //Check if topic is subscribed
        if (this.isTopicSubscribed(topic)) {
            this.#consumer.pause(this.#getTopicPartitions(topic));
            this.emit("paused_topic", topic);
            this.log.debug("Topic paused: " + topic);
        }
        //Throw error if not subscribed
        else shared.throwError(this.log, "Trying to pause a topic that is not subscribed: " + topic);
    }

    //Resume consuming for topic
    resume(topic) {
        //Check if topic is subscribed
        if (this.isTopicSubscribed(topic)) {
            this.#consumer.resume(this.#getTopicPartitions(topic));
            this.emit("resumed_topic", topic);
            this.log.debug("Topic resumed: " + topic);
        }
        //Throw error if not subscribed
        else shared.throwError(this.log, "Trying to resume a topic that is not subscribed: " + topic);
    }

    //Disconnect consumer
    async disconnect() {
        //Check that consumer is connected
        if (this.#state.hasState("CONNECTING")
            || this.#state.hasState("DISCONNECTING")
            || this.#state.hasState("SUBSCRIBING")
            || this.#state.hasState("UNSUBSCRIBING")) await this.#state.waitForStates(["UNSUNSCRIBED", "UNASSIGNED", "ASSIGNED", "DISCONNECTED"]);
        if (!this.#state.hasState("DISCONNECTED")) {
            ///Clear consumer disconnect timeout if exists
            if (this.#disconnectTimeout) {
                clearTimeout(this.#disconnectTimeout);
                this.#disconnectTimeout = null;
            }

            //Unsubscribe if subscribed
            if (this.#state.hasState("ASSIGNED")
                || this.#state.hasState("UNASSIGNED")) await this.#changeSubscriptions([]);

            await this.#state.setState("DISCONNECTING");
            //Disconnect from consumer
            try {
                await new Promise((resolve, reject) => {
                    this.#consumer.disconnect((err) => {
                        if (err) reject(err);
                        else resolve();
                    });
                });
            } catch (err) {
                shared.throwError(this.log, err);
            }
            //Clear #consumer
            this.#consumer = null;
            await this.#state.setState("DISCONNECTED");

            await new Promise((resolve, reject) => {
                this.once("disconnected", () => {
                    resolve();
                })
            });


            this.emit("disconnected", true);
            //Log disconnect
            this.log.info("Disconnected");
        }
        //Throw error if not connected
        else this.log.warn("Trying to disconect a disconnected consumer");
    }

    //Subscribe to topic
    async subscribe(topic, topicConfiguration = {}, handler,) {
        //Check that topic is not subscribed
        if (!this.#topics[topic]) {

            //Clear consumer disconnect timeout if exists
            if (this.#disconnectTimeout) {
                clearTimeout(this.#disconnectTimeout);
                this.#disconnectTimeout = null;
            }

            //Throw error if topic or handler parameter is not set
            if (!topic) shared.throwError(this.log, "ConsumerNodeRDKafka: Topic must be set when calling subscribe.");
            if (!handler) shared.throwError(this.log, "ConsumerNodeRDKafka: Handler must be set when calling subscribe.");

            //Check if consume mode is valid. Throw error if unknown consume mode
            let consumeMode = topicConfiguration?.consumeMode ?? 1; //Default: EARLIEST
            if (!this.#validConsumeModes.includes(consumeMode))
                shared.throwError(this.log, "ConsumerNodeRDKafka: Unknown consumer mode: " + consumeMode);

            let keepOffsetOnReassign = topicConfiguration?.keepOffsetOnReassign ?? true;

            //Create topic state record
            this.#topics[topic] = {
                topic: topic,
                handler: handler,
                consumeMode: consumeMode,
                keepOffsetOnReassign: keepOffsetOnReassign,
                topicConfiguration: topicConfiguration,
                assignments: {}
            }

            //Create list of subscribed topics after adding topic
            const topics = [];
            Object.keys(this.#topics).forEach((key) => {
                topics.push(key);
            })

            //Call #changeSubscriptions to replace subscribed topics list 
            await this.#changeSubscriptions(topics);

            this.emit("subscribed", topic);

            //Log unsubscribe
            this.log.info("Subscribed to topic: " + topic);
        }
        //Trow error if topic is subscribed
        else shared.throwError(this.log, "ConsumerNodeRDKafka: Trying to subscribe to a topic that is subscribed: " + topic);
    }

    //Unsubscribe from topic
    async unsubscribe(topic) {
        //Check if topic is subscribed
        if (this.#topics[topic]) {

            //Create list of subscribed topics after unsubscribe
            let topics = [];
            //If unsubscribe topic given, remove topic, otherwise use empty list
            if (topic)
                Object.keys(this.#topics).forEach((key) => {
                    if (key !== topic)
                        topics.push(key);
                })

            //Call #changeSubscriptions to replace subscribed topics list 
            await this.#changeSubscriptions(topics);

            //Remove topic state
            delete this.#topics[topic];

            //Set disconnect timeout if no subscriptions and still connected
            if (this.isConnected())
                if (!Object.keys(this.#topics).length && this.#consumerIdleDisconnectMs)
                    this.#disconnectTimeout = setTimeout(() => {
                        this.disconnect();
                    }, this.#consumerIdleDisconnectMs);


            this.emit("unsubscribed", topic);

            //Log unsubscribe
            this.log.info("Unsubscribed to topic: " + topic);

        }
        //Warn if trying to unsubscribe from a topic that is not subscribed
        else shared.throwError(this.log, "Trying to unsubscribe from a topic that is not subscribed: " + topic);
    }

    async commit(topic, partition, offset) {
        //Commit a topic
        if (topic) {
            const commits = [];
            //Commit topic/partition to given offset
            if (topic && (partition || partition === 0) && (offset || offset === 0)) {
                commits.push({ topic, partition, offset });
            }
            //Commit topic/partition to last recieved offset
            else if (topic && (partition || partition === 0)) {
                const partitionState = this.#topics[topic].assignments[partition];
                commits.push({ topic, partition, offset: partitionState.lastOffset });
            }
            //Commit all partitions to a topic to last recieved offset
            else if (topic) {
                Object.keys(this.#topics[topic].assignments).forEach((partition) => {
                    const partitionState = this.#topics[topic].assignments[partition];
                    commits.push({ topic, partition: Number(partition), offset: partitionState.lastOffset });
                })
            } else shared.throwError(this.log, "Trying to commit wit insufficient information: " + topic + "/" + partition + " (" + offset + ")");
            const resolves = [];
            const commitResolves = this.#commitResolves;
            commits.forEach((commit) => {
                resolves.push(new Promise((resolve, reject) => {
                    if (!commitResolves[commit.topic]) commitResolves[commit.topic] = {};
                    if (!commitResolves[commit.topic][commit.partition]) commitResolves[commit.topic][commit.partition] = [];
                    commitResolves[commit.topic][commit.partition].push({ offset: commit.offset, resolve: resolve });

                }));
            })
            this.#consumer.commit(commits);
            await Promise.all(resolves);
        }
        //Commit all topics
        else {
            this.#consumer.commit();
        }
    }

    #onCommit(recievedCommits) {
        recievedCommits.forEach((recievedCommit) => {
            const recievedCommitOffset = recievedCommit.offset;
            const commits = this.#commitResolves[recievedCommit.topic][recievedCommit.partition];
            commits.forEach((commit) => {
                if (commit.offset <= recievedCommitOffset) commit.resolve();
            })
        });
    }

    //Change subscriptions of consumer
    async #changeSubscriptions(topics) {
        //Clear consumer disconnect timeout if exists
        if (this.#disconnectTimeout) {
            clearTimeout(this.#disconnectTimeout);
            this.#disconnectTimeout = null;
        }
        //Call #connect to connect if disconnected
        await this.#connect();

        //Remove existing subscriptions
        if (this.#state.hasState("ASSIGNED") || this.#state.hasState("UNASSIGNED")) {
            await this.#state.setState("UNSUBSCRIBING");
            await new Promise((resolve, reject) => {
                //If consumer is assigned unsubscribe is resolved when unassigned event is raised from consumer
                if (this.isAssigned()) {
                    this.once("unassigned", () => {   //?????????????????
                        resolve();
                    });
                    this.#consumer.unsubscribe();
                }
                //Else resolve imediatly
                else {
                    this.#consumer.unsubscribe();
                    resolve()
                }
            });
            await this.#state.setState("UNSUBSCRIBED");
        }
        //Subscribe to ned topic list if there is one
        if (topics.length) {
            await this.#state.setState("SUBSCRIBING");
            this.#consumer.subscribe(topics);
            await this.#state.setState("UNASSIGNED");
        }

        //Remove ongoing subscription change flag
        //this.#changingSubscriptions = false;
    }

    //Connect to consumer if not connected
    async #connect() {
        //Check that consumer is disconnected, skip if not
        if (this.#state.hasState("DISCONNECTED") || this.#state.hasState("DISCONNECTING")) {
            //if(this.#hasState("DISCONNECTING")) await this.#awaitState("DISCONNECTED");
            await this.#state.setState("CONNECTING");
            //Create #consumer
            this.#consumer = new NodeRDKafka.KafkaConsumer(this.#nodeRDKafkaConfig, this.#nodeRDKafkaTopicConfig);
            try {
                await new Promise((resolve, reject) => {
                    this.#consumer.connect(null, (err) => {
                        if (err) reject(err);
                        else resolve();
                    });
                });
            } catch (err) {
                shared.throwError(this.log, err);
            }

            //Handle EOF events
            this.#consumer.on('partition.eof', (eof_event) => {
                //Get topic state
                const topicState = this.#topics[eof_event.topic];
                //Check if partition is not at EOF
                if (!topicState?.assignments[eof_event.partition]?.eofOffset) {
                    //Set partition state eofOffset to last record to recieve
                    topicState.assignments[eof_event.partition].eofOffset = eof_event.offset - 1;
                    //Call #checkEof to see if last record (eof reccord) has been delivered to topic handler
                    this.#checkEof(eof_event.topic);
                }
            });

            //Set #isConnected flag
            //this.#isConnected = true;
            await this.#state.setState("UNSUBSCRIBED");
            //Call #consumeMessages to start consuming messages
            this.#consumeMessages();

            this.log.info("Connected");
        }
    };

    //Returns all partitions for a subscribed topic
    #getTopicPartitions(topic) {
        const topicPartitions = [];
        Object.keys(this.#topics[topic].assignments).forEach((key) => {
            //const topic = topic;
            const partition = Number(key);
            topicPartitions.push({ topic, partition });
        })
        return topicPartitions;
    }

    //Check if EOF of a topic has been reached and raise "eof" event
    #checkEof(topic) {
        //Check if topic is subscribed
        if (this.isTopicSubscribed(topic)) {
            //Get topic state
            const topicState = this.#topics[topic];
            //Skip if topic is EOF
            if (!topicState?.eof) {
                //Check all partitions for EOF
                let eof = true;
                Object.keys(topicState.assignments).forEach(key => {
                    const lastOffset = topicState.assignments[key].lastOffset;
                    const eofOffset = topicState.assignments[key].eofOffset;
                    if (!topicState.assignments[key].eof) {
                        if (eofOffset !== null && lastOffset !== null) {
                            if (!(lastOffset >= eofOffset))
                                eof = false;
                        } else if (eofOffset === null)
                            eof = false;

                        if (eof)
                            topicState.assignments[key].eof = true;
                    }
                })
                //If all partitions of topic is EOF set EOF for topic and emit "eof" event.
                if (eof) {
                    //Set eof flag in topic state
                    topicState.eof = true;

                    //Log topic eof
                    //this.log.info("Topic has consumed up to current offset(s) (EOF): " + topic);

                    //Unsubscribe if TO_END (5) consumeMode, No async await due to lock in consumer loop.
                    let unsubscribeing = false;
                    if (topicState.consumeMode === 5) {
                        this.unsubscribe(topic);
                        unsubscribeing = true
                    }

                    //Emit "eof" event
                    setTimeout(() => {
                        this.emit("eof", topic, unsubscribeing)
                    });

                    this.log.info("EOF reached: " + topic);

                }
            }
        }
        //Throw error if topic is unsubscribed
        else shared.throwError(this.log, "Trying to check EOF on a topic that is not subscribed");
    }

    //Start seek for offset in partition
    async #seek(topic, partition, offset) {

        await this.#state.waitForStates(["ASSIGNED"]);
        //Get topic state
        const topicState = this.#topics[topic];
        //Check if partition is assigned
        if (topicState && topicState.assignments[partition]) {
            //Set seekOffset in partition state
            topicState.assignments[partition].seekOffset = offset;
            //Send seek to consumer with retries if consumer state is not ready after assign
            let retries = 20;
            let seekError = null;
            while (retries)
                await new Promise((resolve, reject) => {
                    this.#consumer.seek({ topic: topic, partition: partition, offset: offset }, 5000, (err) => {
                        if (err) {
                            retries--;
                            if (!retries) seekError = err;
                            resolve()
                            //setTimeout(() => resolve());
                        } else {
                            retries = 0;
                            resolve();
                        }
                    });
                });

            //Throw consumer error if retries fail
            if (seekError)
                shared.throwError(this.log, seekError);
            //this.log.info("Seeked topic/partition for offset: " + topic + "/" + partition + " (" + offset + ")")
        }
        //Throw error if partition is unassigned
        else shared.throwError(this.log, "Trying to check EOF on a topic and partition that is not assigned: " + topic + "/" + partition);
    }

    async #getHighWaterOffset(topic, partition) {
        if (!this.#state.hasState("DISCONNECTED") && !this.#state.hasState("DISCONNECTING") && !this.#state.hasState("CONNECTING")) {
            return await new Promise((resolve, reject) => {
                this.#consumer.queryWatermarkOffsets(topic, partition, 5000, (err, offsets) => {
                    if (err) shared.throwError(this.log, err);
                    resolve(offsets.highOffset);
                });
            })
        }
        else
            shared.throwError(this.log, "Trying to get highWater when not connected: " + topic + "/" + partition);
    }

    //Handles rebalamncing of consumer
    async #onRebalance(err, assignments) {
        //Set #rebalancing flag
        //this.#rebalancing = true;
        //Handle partition assign
        if (err.code === NodeRDKafka.CODES.ERRORS.ERR__ASSIGN_PARTITIONS) {
            //Skip assignment if no asignments
            if (!assignments.length) {
                this.#consumer.assign([]);
                return;
            }

            //Do not assign if state is not UNASSIGNED
            if (!this.#state.hasState("UNASSIGNED")) {
                this.#consumer.unassign();
            }
            else {
                //Setup topic list for assigned partitions 
                const topics = [];

                //Update partition state and topic list for all assigned partitions
                await async.forEach(assignments, async (assignment) => {
                    const highwaterOffset = await this.#getHighWaterOffset(assignment.topic, assignment.partition)
                    //Add topic to topic list if not allready added
                    if (!topics.includes(assignment.topic)) topics.push(assignment.topic);

                    //Create partition state if not existent (not resubscribe or rebalance)
                    if (!this.#topics[assignment.topic]?.assignments[assignment.partition])
                        this.#topics[assignment.topic].assignments[assignment.partition] = {};

                    //Get partition state
                    const partitionState = this.#topics[assignment.topic].assignments[assignment.partition];
                    //Set start values if new partition
                    if (partitionState.assigned === undefined) {
                        partitionState.reassigned = false;
                        partitionState.eof = false;
                        partitionState.eofOffset = null;
                        partitionState.lastOffset = -1;

                    }
                    //Set reassign flag if previously assigned to track last consumed record
                    else {
                        partitionState.reassigned = true;
                    }
                    //Set clear partition state on assignment
                    partitionState.seekOffset = null;
                    //partitionState.seeking = false;
                    partitionState.assigned = true;
                    //Set current highwater offset;
                    partitionState.highwaterOffset = highwaterOffset;

                })

                //Create seek commands based on consumeMode or reassignment for each assigned topics/partitions
                const seeks = [];
                //For each assigned topic
                await async.forEach(topics, async (topic) => {
                    //Get topic state
                    const topicState = this.#topics[topic];
                    //Get consumeMode
                    const consumeMode = topicState.consumeMode;

                    //For each partition in topic state
                    Object.keys(topicState.assignments).forEach(key => {
                        //Get partition state
                        const partitionState = topicState.assignments[key];
                        //if partition is assigned
                        if (partitionState.assigned) {
                            //Setup seekOffset
                            let seekOffset = null;
                            let isNext = false;
                            //On reassign seek to last record + 1
                            if (partitionState.reassigned && partitionState.lastOffset >= 0 && topicState.keepOffsetOnReassign) seekOffset = partitionState.lastOffset + 1;
                            //Else on consume modes EARLIEST=1 or TO_LAST=5 seek to first available offset
                            else if (consumeMode === 1 || consumeMode === 5) seekOffset = 0; //EARLIEST,TO_LAST
                            else if (consumeMode === 3) seekOffset = partitionState.highwaterOffset - 1; //LAST
                            else if (consumeMode === 4) { //NEXT
                                isNext = true;
                                partitionState.eof = true;
                                partitionState.eofOffset = partitionState.highwaterOffset - 1;
                                seekOffset = partitionState.highwaterOffset;
                            }

                            //If partition need seek update partitionstate and add seek command
                            if (seekOffset !== null) {
                                //partitionState.seeking = true;
                                partitionState.seekOffset = seekOffset;
                                //const topic = topicState.topic;
                                const partition = Number(key);
                                const offset = partitionState.seekOffset;
                                seeks.push({ topic, partition, offset, isNext });
                            }
                        }
                    })
                })

                //Log unassignment
                //this.log.info("Partitions assigned");

                //Assign partitions to consumer


                //Run each seek command
                await async.forEach(seeks, async (seek) => {
                    //Call #seek
                    this.#seek(seek.topic, seek.partition, seek.offset);


                    //Check eof if consume mode is NEXT
                    if (seek.isNext)
                        this.#checkEof(seek.topic);
                })



                //Delay seek commands until next loop for assignment to take effect.
                setTimeout(async () => {
                    //this.log.info("Partitions assigned");
                    this.#consumer.assign(assignments);
                    await this.#state.setState("ASSIGNED");
                }, 3000);
            }

        }
        //Handle partition unassign
        else if (err.code === NodeRDKafka.CODES.ERRORS.ERR__REVOKE_PARTITIONS) {
            //Call consumer unassign
            this.#consumer.unassign();
            //Remove assigned flag from all assigned topics/partitions
            Object.keys(this.#topics).forEach(topicKey => {
                Object.keys(this.#topics[topicKey].assignments).forEach(key => {
                    this.#topics[topicKey].assignments[key].assigned = false;
                })
            })
            //Emit "unassigned" event
            this.emit("unassigned");
            if (this.#state.hasState("ASSIGNED"))
                await this.#state.setState("UNASSIGNED");

            //Log unassignment
            //this.log.info("Partitions unassigned");
        }
        //Throw error if unhandled assign code
        else {
            shared.throwError(this.log, err);
        }
    }

    //Start loop to consume batches of records from brokers
    async #consumeMessages() {
        //Log record consume start
        this.log.debug("Starting consuming records: " + this.brokers.toString());

        let handlerPromise = null;

        //Batches will be consumed until consumer is disconnected
        await async.whilst(
            //Repeat while connected
            async () => {
                return (!this.#state.hasState("DISCONNECTED") && !this.#state.hasState("DISCONNECTING"));
            },
            //Get batches and send to handler
            async () => {

                if (handlerPromise) {
                    await handlerPromise;
                    handlerPromise = null;
                }
                //Get batches of records per topic
                const topicBatches = await this.#getTopicBatches();
                //Process batch for each topic in paralell
                handlerPromise = async.forEach(topicBatches, async (batch) => {

                    //Call topic handler to handle batch of records
                    await this.#topics[batch.topic].handler(batch.topic, batch.records);
                    const lastOffset = batch.records[batch.records.length - 1].offset;
                    const topicState = this.#topics[batch.topic];
                    topicState.assignments[batch.partition].lastOffset = lastOffset;

                    //Check for eof all messages processed
                    this.#checkEof(batch.topic);

                    //await this.#eak.yieldEventLoop(); //???????????????????????
                });

                //Fast read loop while assigned
                if (!this.#state.hasState("ASSIGNED")) {
                    // Trottle down if connected but not assigned
                    await new Promise((resolve, reject) => {
                        setTimeout(() => {
                            resolve();
                        }, 1000);
                    });
                }
            }
        );
        //Log record consume end
        this.emit("disconnected", true);
        this.log.debug("Consume loop stopped consuming records");
    }



    async #getTopicBatches() {
        let batchSize = this.batchSize;
        //if (!this.#state.hasState("ASSIGNED")) batchSize = -1;
        const batches = [];
        const records = await new Promise((resolve, reject) => {
            //Get a batch of records from broker if connected. Max number of records defined by config batchSize
            if (!this.isConnected()) resolve([]);
            else
                this.#consumer.consume(batchSize, (err, records) => {
                    if (err) reject(err);
                    else resolve(records);
                });
        });
        //Get batch recieve timestamp
        const now = new Date();
        //Process batch if containing records and consumer is assigned
        if (records.length > 0 && this.#state.hasState("ASSIGNED")) {
            //Setup record batch store
            const topicRecords = {};

            //Process and sort each recieved record
            await async.forEachLimit(records, 1, async (record) => {
                //Check for seek start and skipping of old messages during seek
                let seekSkip = true;
                //Get topic and partition state
                const topicState = this.#topics[record.topic];
                const partitionState = topicState.assignments[record.partition];

                //Detect pre seek records to skip
                if (partitionState.seekOffset !== null && record.offset < partitionState.seekOffset)
                    seekSkip = false;

                //Forward record if not seek skip
                if (seekSkip) {
                    //Reset seekOffset
                    partitionState.seekOffset = null;
                    //Create record item with kafkaHeaders
                    const item = {
                        key: record.key.toString(),
                        offset: record.offset,
                        partition: record.partition,
                        size: record.size,
                        timestamp: new Date(record.timestamp),
                        recieveTimestamp: now,
                        topic: record.topic,
                        record: record.value
                    }
                    //Push item to topics and partitions
                    const partitionKey = record.topic + "-" + record.partition;
                    if (!topicRecords[partitionKey]) topicRecords[partitionKey] = [];
                    topicRecords[partitionKey].push(item);
                }
            });

            //Create batches per topic and partition
            await async.forEachLimit(Object.keys(topicRecords), 1, async (key) => {
                const topic = topicRecords[key][0].topic;
                const partition = topicRecords[key][0].partition;
                const batch = {
                    topic: topic,
                    partition: partition,
                    records: topicRecords[key]
                }
                batches.push(batch);
                //await this.#eak.yieldEventLoop();
            });
        }
        else
            if (records.length > 0 && !this.#state.hasState("ASSIGNED"))
                this.log.debug("SKIP BATCH - Unassigned. Records:" + records.length)

        //Return batches
        return batches;

    }
}

module.exports = NodeRdKafkaConsumer;
