'use strict';

const EventEmitter = require('events');
const clone = require('clone');
const { v4: uuid } = require('uuid');
const shared = require("../shared");

const Topic = require('../Topic');
const Connection = require('./Connection');
const { mainModule } = require('process');
const { Producer } = require('node-rdkafka');


class EasyAsyncKafka extends EventEmitter {

    static Topic=require("../Topic");

    static CleanupPolicies = Object.freeze({ "DELETE": 1, "COMPACT": 2, "COMPACT_DELETE": 3 })
    static ConsumeModes = Object.freeze({ "EARLIEST": 1, "COMMITTED": 2, "LAST": 3, "NEXT": 4, "TO_LAST": 5 })

    defaultTopicConfig = null;
    defaultConnectionConfig = null;

    #log = null;

    #connections = new Map();
    #topics = new Map();
    #topicConnectionLookup = new Map();


    #yieldTimeout = null;


    constructor(defaultTopicConfig, defaultConnectionConfig, logger) {
        super();

        //Setup logger
        if (logger) {
            if (logger.error && logger.warn && logger.info) {
                this.#log = logger;
                this.#log.info("Using custom logger");
            } else {
                this.#log = console;
                this.#log.info("Custom logger does not support log.error, log.warn and/or log.info. Reverting to console");
            }
        } else {
            this.#log = console;
            this.#log.info("Using default logger (console)");
        }

        //Store default topic and connection configs
        if (defaultConnectionConfig) this.defaultTopicConfig = clone(defaultTopicConfig);
        else this.defaultTopicConfig = {};

        if (defaultConnectionConfig) this.defaultConnectionConfig = clone(defaultConnectionConfig)
        else this.defaultConnectionConfig = {};

        //Remove topic from default topic configuration
        if (this.defaultTopicConfig?.topic) delete this.defaultTopicConfig.topic
        // Set default consumer groupId to uuid if not set.
        if (!this.defaultConnectionConfig?.groupId) this.defaultConnectionConfig.groupId = uuid();
        // Set default clientId to groupId if not set.
        if (!this.defaultConnectionConfig?.clientId) this.defaultConnectionConfig.clientId = this.defaultConnectionConfig.groupId;

    }


    async yieldEventLoop() {
        if (!this.#yieldTimeout) {
            this.#yieldTimeout = setTimeout(() => {
                this.#yieldTimeout = null;
            }, 10);
            await new Promise((resolve, reject) => {
                setImmediate(() => resolve());
            });
        }
    }

    async getTopic(topic, topicConfig, connectionConfig) {

        let connection = null;
        let topicObject = null;

        if (!topic) {
            this.#log.error("No topic specified in topicConfig");
            throw ("No topic specified in topicConfig");
        }

        if (!topicConfig) topicConfig = {};
        if (!connectionConfig) connectionConfig = {};

        const mergedTopicConfig = shared.mergeConfig(this.defaultTopicConfig, clone(topicConfig));
        const mergedConnectionConfig = shared.mergeConfig(this.defaultConnectionConfig, clone(connectionConfig));

        if (!mergedConnectionConfig.kafkaBrokers) {
            this.#log.error("No Kafka brokers in connection config or in default connection config");
            throw ("No Kafka brokers in connection config or in default connection config")
        }

        const connectionHash = shared.hashConfig(mergedConnectionConfig);
        const topicHash = topic + shared.hashConfig(mergedTopicConfig) + connectionHash;
        const topicConnectionHash = topic + connectionHash;

        const hasTopic = this.#topics.has(topicHash);
        const hasConnection = this.#connections.has(connectionHash);
        const hasTopicConnection = this.#topicConnectionLookup.has(topicConnectionHash);

        if (hasTopicConnection && !hasTopic) {
            this.#log.error("Multiple topic configurations for same topic on same connection is not supported at this time: "+topic);
            throw ("Multiple topic configurations for same topic on same connection is not supported at this time")
        }
        else if (hasTopic) {
            this.#log.warn("Connection with this topic config exists. Returning existing topic object: "+topic);
            topicObject = this.#topics.get(topicHash);
            connection = this.#connections.get(connectionHash);
        }
        else if (hasConnection && !hasTopic) {
            this.#log.info("Adding topic to existing connection: "+topic);
            connection = this.#connections.get(connectionHash);
            topicObject = new Topic(topic, mergedTopicConfig, connection, this, this.#log);
        }
        else {
            this.#log.info("Creating new connection and adding topic: "+topic);
            connection = new Connection(mergedConnectionConfig, this, this.#log);
            topicObject = new Topic(topic, mergedTopicConfig, connection, this, this.#log);
        }

        this.#connections.set(connectionHash, connection);
        this.#topics.set(topicHash, topicObject);
        this.#topicConnectionLookup.set(topicConnectionHash, true);

        return topicObject;
    };

}

module.exports = EasyAsyncKafka;



async function main() {
    const EAK = new EasyAsyncKafka({
        replicas: 3,
        partitions: 1,
        eakAutoCreateTopic: true,
        eakKeepOffsetOnReassign: true,
        eakHighwater:15000,
        eakLowater: 10000,
        cleanupPolicy: "compact",

    }, {
        kafkaBrokers: ["10.10.0.50:9092"],
        eakProcucerMaxMessagesInFlight: 250
    });

    const topic1 = await EAK.getTopic("test2", {
        segmentBytes: 500000,
        minCleanableDirtyRatio: 0.1,
        deleteRetentionMs: 60000,
        minCompactionLagMs: 60000,
        segmentMs: 60000
    })
    const topic2 = await EAK.getTopic("_prometheus", {
        cleanupPolicy: "delete",
        eakConsumeMode: 1,
        replicas: 3,
        partitions: 1
    })
    const topic3 = await EAK.getTopic("TESTXX", {
        cleanupPolicy: "compact",
        segmentBytes: 10000000,
        minCleanableDirtyRatio: 0.5,
        deleteRetentionMs: 60000,
        minCompactionLagMs: 60000,
        segmentMs: 60000,
        partitions: 1,
        replicas: 3,
    });

    const state=new Map();
    const start=new Date();

    await topic3.consume(async (kafkaHeader, message) => {
        //console.log(message)
        const decodedMessage=JSON.parse(message.toString());
        state.set(kafkaHeader.key,decodedMessage);
        //await topic3.produce(kafkaHeader.key,decodedMessage );
    },true);

    

    const end=new Date();
    console.log("END",((end-start)/1000).toFixed(0)+"s");
await topic3.stopConsume()
    /*await topic1.consume(async (kafkaHeader, message) => {
        //if (!message) console.log(kafkaHeader.topic, kafkaHeader.partition,kafkaHeader.offset, null)
        
        console.log("Message", kafkaHeader.topic, kafkaHeader.partition, kafkaHeader.offset);
        
    });*/

    /*await topic2.consume(async (kafkaHeader, message) => {
        //if (!message) console.log(kafkaHeader.topic, null)
        //else console.log(kafkaHeader.topic, message.toString())
        await new Promise((resolve,reject)=>{
            setTimeout(()=>{
               
                resolve()
            },100)
        })
        console.log("Message", kafkaHeader.topic, kafkaHeader.partition, kafkaHeader.offset)
    });*/


};

main();