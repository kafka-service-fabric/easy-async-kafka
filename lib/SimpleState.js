
class SimpleState {


    state = null;
    configuration = null;
    stateResolves = {};
    log=null;

    constructor(state, configuration,logger) {
        if(!logger) this.log=console;
        else this.log=logger;

        this.configuration = configuration;
        this.state = state;
    }

    hasState(state) {
        return (state === this.state);
    }

    getState(){
        return this.state()
    }

    async setState(state) {
        const fromStates = this.configuration[state];
        if (!fromStates.includes(this.state)) {
            this.log.debug("Pending state change to: " +  state);
            await this.waitForStates(fromStates);
            await this.setState(state);
        } else {
            if(this.stateResolves[state]) this.stateResolves[state].forEach(resolve=>resolve());
            this.log.debug("State changed: " + this.state + ">" + state);
            this.state = state;
        }
    }

    async waitForStates(states) {
        const promises = [];
        states.forEach(state => {
            if (!this.stateResolves[state]) this.stateResolves[state] = [];
            promises.push(new Promise((resolve, reject) => {
                this.stateResolves[state].push(resolve);
            }))
        })
        await Promise.any(promises);
    }
}

module.exports = SimpleState;
