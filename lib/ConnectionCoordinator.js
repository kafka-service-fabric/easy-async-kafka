'use strict';
const clone = require('clone');
const shared = require('./shared');
const async = require("async");
const AdminClient = require("./KafkaJsAdminClient");
const Consumer = require("./NodeRdKafkaConsumer");
const Producer = require("./NodeRdKafkaProducer");

/**
 * Class coordinating reuse and gracefull shutdown of Kafka Consumer, Producer and Admin clients across multiple Topic clients. Used as parameter when constructing instances of Topic class. Most methodes are only for use by Topic class.
 * @class
 * 
*/
class ConnectionCoordinator {

    //Private properties
    #log = null;
    #defaultBrokers = null;
    #defaultConnectionConfiguration = null;
    #clients = null;
    #moduleName = null;

    //Read only properties
    /**
     * Default brokers set for ConnectionCoordinator.
     * @readonly
     */
    get defaultBrokers() { return this.#defaultBrokers };
    /**
     * Default connection configuration set for ConnectionCoordinator.
     * @readonly
     */
    get defaultConnectionConfiguration() { return this.#defaultConnectionConfiguration };
    //get clients() { return this.#clients };
    //get moduleName() { return this.#moduleName };

    constructor(defaultBrokers, defaultConnectionConfiguration, logger, parentName = null) {

        if (parentName) this.#moduleName = parentName + "/" + "ConnectionCoordinator";
        else this.#moduleName = "EasyAsyncKafka/ConnectionCoordinator";

        if (!logger?.info && logger?.log) logger = logger.log;
        if (!(logger?.error && logger?.warn && logger?.info && logger?.debug)) {
            this.#log = console;
            this.#log.info("Custom logger does not support log.error, log.warn and/or log.info. Reverting to console");
        }
        else if (logger?.child) this.#log = logger.child({ module: this.#moduleName });
        else this.#log = logger;

        this.#defaultBrokers = defaultBrokers ?? null;
        this.#defaultConnectionConfiguration = defaultConnectionConfiguration ?? {};
        this.#clients = {
            producers: {},
            consumers: {},
            adminClients: {}
        };
        this.#log.info("Initialized");
    }

    /** 
     * Disconnect from all clients coordinated by this instance of ConnectionCoordinator.
     * @async
     * @example
     * connectionCoordinator.disconnect();
     */
    async disconnect() {
        await async.forEach(Object.values(this.#clients.producers), async (producers) => {
            await async.forEach(Object.values(producers), async (producer) => {
                await producer.disconnect();
            });
        });
        await async.forEach(Object.values(this.#clients.consumers), async (consumers) => {
            await async.forEach(Object.values(consumers), async (consumer) => {
                if (consumer.isConnected()) await consumer.disconnect();
            });
        });
        await async.forEach(Object.values(this.#clients.adminClients), async (adminClients) => {
            await async.forEach(Object.values(adminClients), async (adminClient) => {
                await adminClient.disconnect();
            });
        });
        this.#log.info("Disconnected all kafka clients");
        //await async.forEach(Object.values(this.clients.consumers), async (consumer) => { await this.clients.consumers[consumer].disconnect(); });
        //await async.forEach(Object.values(this.clients.adminClients), async (adminClient) => { await this.clients.adminClients[adminClient].disconnect(); });
    }

    /** 
     * Create or reuse a Kafka consumer client. If brokers and/or connectionConfiguration is not set defaultBrokers and/or defaultConnectionConfiguration is used.
     * @async
     * @param {string[]} [brokers=[defaultBrokers]] Specifies Kafka broker to use, if empty default broker is used
     * @param {Object} [connectionConfiguration=defaultConnectionConfiguration] Specifies Kafka connection configuration to use, if empty default connection configuration is used
     * @returns {NodeRDKafkaConsumer}
     * @example
     * connectionCoordinator.disconnect();
     */

    getConsumer(brokers, connectionConfiguration, logContext = null) {
        if (logContext) logContext = logContext + "> ";
        else logContext = "";

        if (!brokers && this.#defaultBrokers) brokers = this.#defaultBrokers;
        else if (!brokers) shared.throwError(this.#log, "No brokers or defalt brokers spesified for consumer client");

        const brokerHash = brokers.sort().toString();
        if (!this.#clients?.consumers[brokerHash]) this.#clients.consumers[brokerHash] = {};

        const consumers = this.#clients.consumers[brokerHash];

        const mergedConnectionConfig = shared.mergeConfig(this.#defaultConnectionConfiguration, clone(connectionConfiguration));
        const configurationHash = shared.hashConfig(mergedConnectionConfig);

        if (!consumers[configurationHash]) {
            this.#log.info("No consumer with existing configuration found. Creating new consumer: " + brokers.toString());
            this.#log.debug("Consumer configuration: " + brokers.toString() + " - " + JSON.stringify(mergedConnectionConfig));
            consumers[configurationHash] = new Consumer(brokers, mergedConnectionConfig, this.#log, this.#moduleName);
        } else {
            this.#log.info("Reusing consumer with same configuration: " + brokers.toString());
            this.#log.debug("Consumer configuration: " + brokers.toString() + " - " + JSON.stringify(mergedConnectionConfig));
        }

        return consumers[configurationHash];
    }

    getProducer(brokers, connectionConfiguration, logContext = null) {
        if (logContext) logContext = logContext + "> ";
        else logContext = "";

        if (!brokers && this.#defaultBrokers) brokers = this.#defaultBrokers;
        else if (!brokers) shared.throwError(this.#log, "No brokers or defalt brokers spesified for producer client");

        const brokerHash = brokers.sort().toString();
        if (!this.#clients?.producers[brokerHash]) this.#clients.producers[brokerHash] = {};

        const producers = this.#clients.producers[brokerHash];

        const mergedConnectionConfig = shared.mergeConfig(this.#defaultConnectionConfiguration, clone(connectionConfiguration));
        const configurationHash = shared.hashConfig(mergedConnectionConfig);

        if (!producers[configurationHash]) {
            this.#log.info("No producer with existing configuration found. Creating new producer: " + brokers.toString());
            this.#log.debug("Producer configuration: " + brokers.toString() + " - " + JSON.stringify(mergedConnectionConfig));
            producers[configurationHash] = new Producer(brokers, mergedConnectionConfig, this.#log, this.#moduleName);
        } else {
            this.#log.info("Reusing producer with same configuration: " + brokers.toString());
            this.#log.debug("Producer configuration: " + brokers.toString() + " - " + JSON.stringify(mergedConnectionConfig));
        }

        return producers[configurationHash];
    }

    getAdminClient(brokers, connectionConfiguration, logContext = null) {
        if (logContext) logContext = logContext + "> ";
        else logContext = "";

        if (!brokers && this.#defaultBrokers) brokers = this.#defaultBrokers;
        else if (!brokers) shared.throwError(this.#log, logContext + "No brokers or defalt brokers spesified for adminClient client");

        const brokerHash = brokers.sort().toString();
        if (!this.#clients?.adminClients[brokerHash]) this.#clients.adminClients[brokerHash] = {};

        const adminClient = this.#clients.adminClients[brokerHash];

        const mergedConnectionConfig = shared.mergeConfig(this.#defaultConnectionConfiguration, clone(connectionConfiguration));
        const configurationHash = shared.hashConfig(mergedConnectionConfig);

        if (!adminClient[configurationHash]) {
            this.#log.info(logContext + "No admin client with existing configuration found. Creating new admin client: " + brokers.toString());
            this.#log.debug(logContext + "Admin client configuration: " + brokers.toString() + " - " + JSON.stringify(mergedConnectionConfig));
            adminClient[configurationHash] = new AdminClient(brokers, mergedConnectionConfig, this.#log, this.#moduleName);
        } else {
            this.#log.info(logContext + "Reusing admin client with same configuration: " + brokers.toString());
            this.#log.debug(logContext + "Admin client configuration: " + brokers.toString() + " - " + JSON.stringify(mergedConnectionConfig));
        }

        return adminClient[configurationHash];
    }

}

module.exports = ConnectionCoordinator;
