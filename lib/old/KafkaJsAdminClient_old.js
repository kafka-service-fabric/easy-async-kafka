'use strict';

const EventEmitter = require('events');
const async = require("async");
const shared = require("./shared");
const { ConfigResourceTypes } = require('kafkajs')

class KafkaJsAdminClient extends EventEmitter {

    #log = null;
    #eak = null;
    #connection = null;
    #kafkaJsConnectionConfig = null;
    #adminClient = null;
    #disconnectTimeout = null;
    #changingConnectionState = null;
    //#brokerTopics = null;

    constructor(connectionConfig, connection, eak, logger) {
        super();


        this.#log = logger;
        this.#eak = eak;
        this.#connection = connection;
        this.#changingConnectionState = false;

        this.#kafkaJsConnectionConfig = {
            clientId: connectionConfig.clientId,
            brokers: connectionConfig.kafkaBrokers
        }
    }

    async createTopic(topic, partitions, replicas, topicConfig) {
        await this.#connect();

        const configEntries = [];
        Object.keys(topicConfig).forEach((key) => {
            configEntries.push({ name: key, value: topicConfig[key].toString() });
        });

        await this.#adminClient.createTopics({
            waitForLeaders: true,
            topics: [{
                topic: topic,
                numPartitions: partitions,
                replicationFactor: replicas,
                configEntries: configEntries
            }]
        });

        this.#log.info("Created topic on broker: " + topic);
    }



    async getBrokerTopics() {
        await this.#connect();
        return await this.#adminClient.listTopics();
    }

    async getBrokerTopicConfig(topic) {
        await this.#connect();
        const brokerTopicConfig = {};
        const config = await this.#adminClient.describeConfigs({
            includeSynonyms: false,
            resources: [
                {
                    type: ConfigResourceTypes.TOPIC,
                    name: topic
                }
            ]
        })
        if (config.resources.length === 1)
            if (config.resources[0].errorCode === 0) {
                config.resources[0].configEntries.forEach((entry) => {
                    if (!entry.readOnly)
                        brokerTopicConfig[shared.dotToCamelCase(entry.configName)] = entry.configValue;
                });
            }
        let topicMetadata = null;
        try {
            await this.#connect();
            topicMetadata = await this.#adminClient.fetchTopicMetadata({ topics: [topic] });
        } catch (err) {
            this.#log.warn("getTopicMetadata: Topic does not exist on server: " + topic);
        }
        if (topicMetadata?.topics[0]?.partitions) {
            brokerTopicConfig.replicas = topicMetadata.topics[0].partitions[0].replicas.length.toString();
            brokerTopicConfig.partitions = topicMetadata.topics[0].partitions.length.toString();
        }
        return brokerTopicConfig;
    }


    async #connect() {
        if (this.#changingConnectionState) {
            await async.whilst(
                async () => {
                    return this.#changingConnectionState;
                },
                async () => {
                    await new Promise((resolve, reject) => {
                        setTimeout(() => resolve(), 10);
                    })
                },
            )
        }
        if (!this.#adminClient) {
            this.#changingConnectionState = true;
            const { Kafka } = require('kafkajs');
            this.#adminClient = (new Kafka(this.#kafkaJsConnectionConfig)).admin();
            await this.#adminClient.connect();
            this.#log.info("Admin client connected");
            this.#changingConnectionState = false;
        }

        if (this.#disconnectTimeout) clearTimeout(this.#disconnectTimeout);
        this.#disconnectTimeout = setTimeout(async () => {
            await this.#disconnect()
            this.#disconnectTimeout = null;
            this.#adminClient = null;
            this.#log.info("Admin client disconnected (inactive)");
        }, 15000)
    }

    async #disconnect() {
        if (this.#changingConnectionState) {
            await async.whilst(
                async () => {
                    return this.#changingConnectionState;
                },
                async () => {
                    await new Promise((resume, reject) => {
                        setTimeout(() => resolve(), 10);
                    })
                },
            )
        }
        if (this.#adminClient) {
            this.#changingConnectionState = true;
            await this.#adminClient.disconnect();
            this.#changingConnectionState = true;
        }
    }
}

module.exports = KafkaJsAdminClient;
