'use strict';

const EventEmitter = require('events');
const async = require('async');
const clone = require('clone');
const NodeRDKafka = require('node-rdkafka');

class NodeRdKafkaProducer extends EventEmitter {

    #nodeRDKafkaConfig = null;
    #nodeRDKafkaTopicConfig = null;
    #producer = null;
    #connection = null;
    #log = null;
    #eak = null;
    #nextToken = null;
    #tokensInFlight = null;
    #inFlightResolves = null;
    #clientTimeout = null;

    #ackResolves = null;

    #changingConnectionState = null;

    constructor(connectionConfig, connection, eak, logger) {
        super();

        this.#log = logger;
        this.#eak = eak;
        this.#connection = connection;

        this.#changingConnectionState = false;
        this.#nextToken = 0;
        this.#tokensInFlight = {};
        this.#ackResolves = {};
        this.maxTokensInFlight = connectionConfig.eakProcucerMaxMessagesInFlight || 200;
        this.batchSize = Math.round(this.maxTokensInFlight * 0.2);
        if (this.batchSize < 1) this.batchSize = 1;

        this.lowater = Math.round(this.maxTokensInFlight * 0.2)+1;
        if (this.lowater < 1) this.lowater = 1;


        this.#inFlightResolves = [];

        this.#nodeRDKafkaConfig = {
            'client.id': connectionConfig.clientId,
            'message.max.bytes': connectionConfig.messageMaxBytes || 1000000000,
            'metadata.broker.list': connectionConfig.kafkaBrokers,
            'max.in.flight.requests.per.connection': connectionConfig.maxInFlightRequestsPerConnection || 1,
            'retry.backoff.ms': connectionConfig.retryBackoffMs || 10,
            'message.send.max.retries': connectionConfig.messageSendMaxRetries || 10,
            'socket.keepalive.enable': connectionConfig.socketKeepaliveEnable || true,
            'queue.buffering.max.messages': connectionConfig.queueBufferingMaxMessages || this.maxTokensInFlight * 2,
            'linger.ms': connectionConfig.lingerMs || 100,
            'batch.num.messages': connectionConfig.batchNumMessages || this.batchSize,
            'dr_cb': true
        };
        this.#nodeRDKafkaTopicConfig = {
            'request.required.acks': 1,
            //'compression.codec': this.#producerConfig.compressionCodec
        }


    }

    async send(topic, key, message, awaitConfirmation) {
        await this.#connect();

        const token = this.#nextToken;
        this.#nextToken++;

        this.#tokensInFlight[token] = true;
        if (Object.keys(this.#tokensInFlight).length > this.maxTokensInFlight) {
            //if (!this.#inFlightResolves.length) console.log("Producer paused");
            await new Promise((resolve, reject) => {
                this.#inFlightResolves.push(resolve);
            })

        }

        this.#producer.produce(topic, null, Buffer.from(message), key, null, token);
        if (awaitConfirmation) {
            await new Promise((resolve, reject) => {
                this.#ackResolves[token] = resolve;
            })
        }


    }

    #handleDeliveryReport(err, report) {
        if (err) throw (err);
        const self=this;
        if (self.#tokensInFlight[report.opaque]) {
            //console.log(report);
            //console.log(self.#tokensInFlight);

            delete self.#tokensInFlight[report.opaque];

            if(self.#ackResolves[report.opaque]){
                self.#ackResolves[report.opaque]();
                delete self.#ackResolves[report.opaque];
            }

            const inFlight = Object.keys(self.#tokensInFlight).length
            if (self.#inFlightResolves.length && inFlight <= self.lowater) {
                while (self.#inFlightResolves.length) {
                    const resolve = self.#inFlightResolves.shift();
                    if (self.#inFlightResolves.length) resolve(false);
                    else resolve(true);
                }
                self.#inFlightResolves.forEach((resolve) => resolve());
                self.#inFlightResolves = [];
            }

        } else {
            throw ("Token missmatch");
        }
    }

    async #connect() {
        if (this.#changingConnectionState) {
            await async.whilst(
                async () => {
                    return this.#changingConnectionState;
                },
                async () => {
                    await new Promise((resolve, reject) => {
                        setTimeout(() => resolve(), 10);
                    })
                },
            )
        }
        if (!this.#producer) {
            this.#changingConnectionState = true;

            this.#producer = new NodeRDKafka.Producer(this.#nodeRDKafkaConfig, this.#nodeRDKafkaTopicConfig);

            await new Promise((resolve, reject) => {
                this.#producer.connect((err) => {
                    if (err) reject();
                });

                this.#producer.once('ready', () => { resolve() });
            });

            this.#producer.setPollInterval(1);

            const self = this;
            this.#producer.on('delivery-report', function (err, report) {
                self.#handleDeliveryReport(err, report);
            });

            this.#log.info("Producer client connected");
            this.#changingConnectionState = false;
        }
        if (this.#clientTimeout) clearTimeout(this.#clientTimeout);
        this.#clientTimeout = setTimeout(() => {
            this.#log.info("Producer client idle timeout");
            this.#disconnect();
        }, 30000);

    }

    async #disconnect() {
        if (this.#changingConnectionState) {
            await async.whilst(
                async () => {
                    return this.#changingConnectionState;
                },
                async () => {
                    await new Promise((resolve, reject) => {
                        setTimeout(() => resolve(), 10);
                    })
                },
            )
        }
        if (this.#producer) {
            this.#changingConnectionState = true;

            await new Promise((resolve, reject) => {
                this.#producer.disconnect((err) => {
                    if (err) reject();
                });
                this.#producer.once('disconnected', () => { resolve() });
            });

            this.#producer = null;
            this.#log.info("Producer client disconnected");
            this.#changingConnectionState = true;
        }
    }

}

module.exports = NodeRdKafkaProducer;