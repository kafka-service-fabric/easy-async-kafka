'use strict';

const EventEmitter = require('events');
const Consumer = require('./NodeRdKafkaConsumer_old');
const Producer = require('./NodeRdKafkaProducer_old');
const AdminClient = require('./KafkaJsAdminClient_old');
const clone = require('clone');
const async = require('async');
const { rejects } = require('assert');
const { resolve } = require('path');


class Connection extends EventEmitter {

    connectionConfig = null;

    #log = null;
    #eak = null;

    #adminClient = null;
    #consumerClient = null;
    #producerClient = null;
    #addingConsumer = null;
    #connectingAdminClient = null;

    #topics = null;

    //#brokerTopics = null;
    //#registeredTopics = null;

    constructor(connectionConfig, eak, logger) {
        super();

        this.#log = logger;
        this.#eak = eak;
        if (connectionConfig) this.connectionConfig = connectionConfig;
        else this.connectionConfig = {};

        if (!this.connectionConfig.eakBatchSize) this.connectionConfig.eakBatchSize = 500;

        this.#topics = new Map();
        this.#addingConsumer = false
        this.#connectingAdminClient = false
        
        //this.#brokerTopics = new Map();
    }

    async consume(topicObject) {
        const topic = topicObject.topicState.topic;

        if (this.#topics.has(topic)) {
            this.#log.error("Topic has a registered handler on this connection. Unregister handler before registring handler again (NOT IMPLEMENTED): " + topic);
            throw ("Topic has a registered handler on this connection. Unregister handler before registring handler again (NOT IMPLEMENTED: " + topic);
        }
        else {
            if (this.#addingConsumer) {
                await async.whilst(
                    async () => {
                        return this.#addingConsumer;
                    },
                    async () => {
                        await new Promise((resume, reject) => {
                            setTimeout(() => resolve(), 100);
                        })
                    },
                )
            }
            this.#addingConsumer = true;
            //Create consumer client
            await this.#connectConsumerClient();

            this.#topics.set(topic, topicObject);

            await this.#consumerClient.subscribe(topicObject.topicState);
            this.#log.info("Connections subscribed to topic: " + topic);
            this.#addingConsumer = true;
        }
    }

    async stopConsume(topicObject) {
        const topic = topicObject.topicState.topic;

        if (!this.#topics.has(topic)) {
            this.#log.error("Topic does not have a registered handleron this connection: " + topic);
            throw ("Topic does not have a registered handleron this connection: " + topic);
        }
        else {
            await this.#consumerClient.unsubscribe(topic);
            this.#topics.delete(topic);
            this.#log.info("Connection unsubscribed from topic: " + topic);
        }
    }

    async pause(topic) {
        await this.#consumerClient.pause(topic);
    }

    async resume(topic) {
        await this.#consumerClient.resume(topic);
    }

    async commit(topic, partition, offset) {

    }

    async produce(topic, key, message,awaitConfirmation) {
        await this.#connectProducerClient();

        await this.#producerClient.send(topic, key, message,awaitConfirmation);

    }


    async brokerHasTopic(topic) {
        await this.#connectAdminClient();
        const brokerTopics = await this.getBrokerTopics()
        if (brokerTopics.includes(topic)) return true;
        else return false;
    }

    async getBrokerTopicConfig(topic) {
        await this.#connectAdminClient();
        return await this.#adminClient.getBrokerTopicConfig(topic);
    }

    async getBrokerTopicOffsets(topic) {
        await this.#connectAdminClient();
        return await this.#adminClient.getTopicOffsets(topic);
    }

    async getBrokerTopics(topic) {
        await this.#connectAdminClient();
        return await this.#adminClient.getBrokerTopics(topic);
    }

    async createBrokerTopic(topic, topicConfig) {
        //Clone config to prevent state manipulation
        const config = clone(topicConfig);

        //Extract partition count and replication count from config
        const partitions = config.partitions;
        delete config.partitions;
        const replicas = config.replicas;
        delete config.replicas;

        //Map config to kafka config (camelCase to dotted)
        const kafkaTopicConfig = {};
        Object.keys(config).forEach((key) => {
            if (key.substr(0, 3) !== "eak") {
                const kafkaKey = key.split(/(?=[A-Z])/).map(s => s.toLowerCase()).join('.');
                kafkaTopicConfig[kafkaKey] = config[key];
            }
        });

        await this.#connectAdminClient();
        await this.#adminClient.createTopic(topic, partitions, replicas, kafkaTopicConfig);
    }

    async #connectAdminClient() {

        if (!this.#adminClient) {
            this.#adminClient = new AdminClient(this.connectionConfig, this, this.#eak, this.#log);
        }
    }

    async #connectConsumerClient() {
        if (!this.#consumerClient) {
            this.#consumerClient = new Consumer(this.connectionConfig, this, this.#eak, this.#log);
        }
    }

    async #connectProducerClient() {
        if (!this.#producerClient) {
            this.#producerClient = new Producer(this.connectionConfig, this, this.#eak, this.#log);
        }
    }

    async _recieveMessages(partitionKey, items) {
        const topic = partitionKey.split("-")[0];
        const topicObject = this.#topics.get(topic);
        if (topicObject._recieveMessages && items) await topicObject._recieveMessages(items);
        else if (messages) {
            this.#log.error("Recieving messages from topic without handler");
            throw ("Recieving messages from topic without handler");
        }
        await this.#eak.yieldEventLoop();
    }
}

module.exports = Connection;