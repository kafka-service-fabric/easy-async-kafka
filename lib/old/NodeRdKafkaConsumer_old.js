'use strict';

const EventEmitter = require('events');
const async = require('async');
const clone = require('clone');
const NodeRDKafka = require('node-rdkafka');
const { resolve } = require('path');
const { isNull } = require('util');

class NodeRdKafkaConsumer extends EventEmitter {

    #nodeRDKafkaConfig = null;
    #nodeRDKafkaTopicConfig = null;
    #consumer = null;
    #connection = null;
    #log = null;
    #eak = null;
    #topics = null;
    #assigned = null;
    #rebalancing = null;
    #connected = null;
    #changingSubscriptions = null;
    #disconnectTimeout = null;

    constructor(connectionConfig, connection, eak, logger) {
        super();

        this.#log = logger;
        this.#eak = eak;
        this.#connection = connection;
        this.#topics = {};
        this.#assigned = false;
        this.#rebalancing = false;
        this.#changingSubscriptions = false;
        this.#connected = false;

        this.#nodeRDKafkaConfig = {
            'group.id': connectionConfig.groupId,
            'metadata.broker.list': connectionConfig.kafkaBrokers,
            "socket.keepalive.enable": true,
            "enable.auto.commit": false,
            //"auto.commit.interval.ms": this.#consumerConfiguration.commitIntervalMs,
            "enable.partition.eof": true,
            "enable.auto.offset.store": false,
            "fetch.wait.max.ms": connectionConfig.maxWaitMs || 10,
            'rebalance_cb': (err, assignments) => {
                this.#onRebalance(err, assignments)
            },
            /*'offset_commit_cb': (err, topicPartitions) => {
                this.#log.info("NodeRDKafka: Topics committed to broker")
                //this.#onCommit(err, topicPartitions);
            }*/
        }
        this.#nodeRDKafkaTopicConfig = {
            "auto.offset.reset": "beginning",
        }
    }

    isConnected() {
        return this.#connected;
    }

    hasSubscriptions() {
        if (this.#topics.size()) return true;
        else return false;
    }

    isAssigned() {
        if (this.#assigned) return true;
        else return false;
    }

    async pause(topic) {
        this.#consumer.pause(this.#getTopicPartitions(topic));
    }

    async resume(topic) {
        this.#consumer.resume(this.#getTopicPartitions(topic));
    }

    #getTopicPartitions(topic){
        const topicPartitions=[];
        Object.keys(this.#topics[topic].assignmentStates).forEach((key)=>{
            const keyParts = key.split("-");
            const topic = keyParts[0];
            const partition = Number(keyParts[1]);
            topicPartitions.push({ topic, partition }); 
        })
        return topicPartitions;
    }

    async #connect() {
        if (!this.isConnected()) {

            this.#consumer = new NodeRDKafka.KafkaConsumer(this.#nodeRDKafkaConfig, this.#nodeRDKafkaTopicConfig);
            try {
                await new Promise((resolve, reject) => {
                    this.#consumer.connect(null, (err) => {
                        if (err) reject(err);
                        else resolve();
                    });
                });
            } catch (err) {
                throw (err);
            }
            this.#consumer.on('partition.eof', (eof_event) => {

                const topicState = this.#topics[eof_event.topic];
                const partitionKey = eof_event.topic + "-" + eof_event.partition;
                if (!topicState.assignmentStates[partitionKey]?.eofOffset) {
                    topicState.assignmentStates[partitionKey].eofOffset = eof_event.offset - 1;
                    //console.log(eof_event);
                    this.#checkEof(eof_event.topic);
                }
            });
            this.#connected = true;
            this.#consumeMessages();
            this.#log.info("NodeRDKafka: Consumer is connected");
        }
    };

    async disconnect() {

        if (this.isConnected()) {
            this.#connected = false;
            try {
                await new Promise((resolve, reject) => {
                    this.#consumer.disconnect((err) => {
                        if (err) reject(err);
                        else resolve();
                    });
                });
            } catch (err) {
                throw (err);
            }
            this.#consumer = null;
            this.#log.info("NodeRDKafka: Consumer is disconnected");
        }

    }

    async subscribe(topicState) {

        topicState.assignmentStates = {};
        this.#topics[topicState.topic] = topicState;

        const topics = [];
        Object.keys(this.#topics).forEach((key) => {
            topics.push(key);
        })
        await this.#changeSubscriptions(topics);

        this.#log.info("NodeRDKafka: Consumer is subscribed to topic: " + topicState.topic);
    }

    async unsubscribe(topic) {

        if (this.#topics[topic]) {
            const topics = [];
            Object.keys(this.#topics).forEach((key) => {
                if (key !== topic)
                    topics.push(key);
            })
            await this.#changeSubscriptions(topics);
            //remove assignmentstates
            this.#topics[topic].assignmentStates = {};
            //
            delete this.#topics[topic];
            this.#log.info("NodeRDKafka: Consumer is unsubscribed from topic: " + topic);
            if (!Object.keys(this.#topics).length)
                this.#disconnectTimeout = setTimeout(() => {
                    this.disconnect();
                }, 15000);
        }
    }

    async #changeSubscriptions(topics) {
        if (this.#disconnectTimeout) {
            clearTimeout(this.#disconnectTimeout);
            this.#disconnectTimeout = null;
        }
        await this.#connect();
        await async.whilst(async () => { return (this.#changingSubscriptions || this.#rebalancing) }, async () => {
            await new Promise((resolve, reject) => {
                setTimeout(() => resolve(), 100);
            });
        });
        this.#changingSubscriptions = true;
        if (this.hasSubscriptions) {
            await new Promise((resolve, reject) => {
                //If consumer is assigned unsubscribe is resolved when consumerStatus changes to CONNECTED
                if (this.isAssigned()) {
                    this.#consumer.unsubscribe();
                    resolve();
                    /*this.once("unassigned", () => {
                        resolve();
                    });*/
                } else {
                    this.#consumer.unsubscribe();
                    resolve()
                }
            });
        }
        if (topics.length)
            this.#consumer.subscribe(topics);

        this.#changingSubscriptions = false;
    }

    async #checkEof(topic) {
        const topicState = this.#topics[topic];
        if (!topicState.eof) {
            let eof = true;
            Object.keys(topicState.assignmentStates).forEach(key => {
                const lastOffset = topicState.assignmentStates[key].lastOffset;
                const eofOffset = topicState.assignmentStates[key].eofOffset;
                if (!topicState.assignmentStates[key].eof) {
                    if (eofOffset !== null && lastOffset !== null) {
                        if (!(lastOffset >= eofOffset))
                            eof = false;
                    } else if (eofOffset === null)
                        eof = false;

                    if (eof)
                        topicState.assignmentStates[key].eof = true;
                }
            })
            if (eof) {
                topicState.eof = true;
                await this.#connection._recieveMessages(topic, { event: { type: "eof", topic: topic } });

                //Unsubscribe if TO_END mode
                if (topicState.topicConfig.eakConsumeMode === 5) {
                    await this.unsubscribe(topic);
                    //await this.#connection._recieveMessages(topic, { event: { type: "eof_disconnect", topic: topic } });
                }
            }
        }
    }

    async #seek(topic, partition, offset) {
        const topicState = this.#topics[topic];
        topicState.assignmentStates[topic + "-" + partition].seekOffset = offset;
        await new Promise((resolve, reject) => {
            this.#consumer.seek({ topic: topic, partition: partition, offset: offset }, 5000, (err) => {
                resolve();
            });
        })

    }

    async #onRebalance(err, assignments) {
        this.#rebalancing = true;
        if (err.code === NodeRDKafka.CODES.ERRORS.ERR__ASSIGN_PARTITIONS) {
            const topics = [];
            //const assignmentStates = {};
            assignments.forEach(assignment => {
                const partitionKey = assignment.topic + "-" + assignment.partition;

                if (!topics.includes(assignment.topic)) topics.push(assignment.topic);

                if (!this.#topics[assignment.topic]?.assignmentStates[partitionKey]) this.#topics[assignment.topic].assignmentStates[partitionKey] = {};
                const partitionState = this.#topics[assignment.topic].assignmentStates[partitionKey];
                if (partitionState.assigned === undefined) {
                    partitionState.reassigned = false;
                    partitionState.eof = false;
                    partitionState.eofOffset = null;
                    partitionState.lastOffset = -1;
                } else {
                    partitionState.reassigned = true;

                }
                partitionState.seekOffset = null;
                partitionState.seeking = false;
                partitionState.assigned = true;

            })
            const seeks = [];
            await async.forEachLimit(topics, 1, async (topic) => {
                const topicState = this.#topics[topic];

                //Re-assignment
                Object.keys(topicState.assignmentStates).forEach(key => {
                    const consumeMode = topicState.topicConfig.eakConsumeMode;

                    const partitionState = topicState.assignmentStates[key];
                    let seekOffset = null;
                    if (partitionState.reassigned && partitionState.lastOffset >= 0 && topicState.topicConfig.eakKeepOffsetOnReassign) seekOffset = partitionState.lastOffset + 1;
                    else if (consumeMode === 1 || consumeMode === 5) seekOffset = 0; //EARLIEST,TO_LAST
                    if (seekOffset !== null) {
                        partitionState.seeking = true;
                        partitionState.seekOffset = seekOffset;
                        const keyParts = key.split("-");
                        const topic = keyParts[0];
                        const partition = Number(keyParts[1]);
                        const offset = partitionState.lastOffset + 1;
                        seeks.push({ topic, partition, offset });
                    }

                })
            })

            this.#consumer.assign(assignments);
            this.#assigned = true;
            this.#log.info("NodeRDKafka: Partitions assigned");

            //Delay seek command until next loop for assignment to take effect.
            setTimeout(async () => {
                await async.forEachLimit(seeks, 1, async (seek) => {
                    await this.#seek(seek.topic, seek.partition, seek.offset);
                })
            })
        }
        else if (err.code === NodeRDKafka.CODES.ERRORS.ERR__REVOKE_PARTITIONS) {
            this.#assigned = false;
            this.#consumer.unassign();
            Object.keys(this.#topics).forEach(topicKey => {
                Object.keys(this.#topics[topicKey].assignmentStates).forEach(key => {
                    this.#topics[topicKey].assignmentStates[key].assigned = false;
                })
            })
            this.emit("unassigned");
            this.#log.info("NodeRDKafka: Partitions unassigned");
        }
        else {
            throw (err);
        }
        this.#rebalancing = false;
    }

    async #consumeMessages() {
        //Slow read loop waiting for assignment
        this.#log.info("NodeRDKafka: Consumer started");
        await async.whilst(
            //Repeat while connected
            async () => {
                return (this.isConnected());
            },
            //While worker is assigned get batches of messages to fill decodeQueue
            async () => {

                //Get messages
                const topicBatches = await this.#getTopicBatches();
                //Process messages
                await async.forEachLimit(topicBatches, 1, async (batch) => {
                    await this.#connection._recieveMessages(batch.topic, batch.messages);
                    const lastOffset = batch.messages[batch.messages.length - 1].kafkaHeader.offset;
                    const topicState = this.#topics[batch.topic];
                    topicState.assignmentStates[batch.partitionKey].lastOffset = lastOffset;

                    //Check for eof all messages processed
                    await this.#checkEof(batch.topic);

                    await this.#eak.yieldEventLoop();
                });

                //Fast read loop while assigned
                if (!this.isAssigned()) {
                    // Trottle down if connected but not assigned
                    await new Promise((resolve, reject) => {
                        setTimeout(() => {
                            resolve();
                        }, 1000);
                    });
                }
            }

        );
        this.#log.info("NodeRDKafka: Consumer stopped");
    }

    async #getTopicBatches() {
        const batches = [];
        const messages = await new Promise((resolve, reject) => {
            const batchSize = this.#connection.connectionConfig.eakBatchSize;
            this.#consumer.consume(batchSize, (err, messages) => {
                if (err) reject(err);
                else resolve(messages);
            });
        });

        const now = new Date();
        if (messages.length > 0) {
            const topicMessages = {};
            await async.forEachLimit(messages, 1, async (message) => {
                const topicState = this.#topics[message.topic];

                //Check for seek start and skipping of old messages during seek
                let forward = true;
                const partitionState = topicState.assignmentStates[message.topic + "-" + message.partition];
                //Start sheduled seek
                if (partitionState.seekOffset !== null && partitionState.seeking === false) {
                    //await this.#seek(message.topic, message.partition, partitionState.seekOffset);
                    //partitionState.seeking = true;
                    forward = false;
                } else if (partitionState.seekOffset !== null)
                    if (message.offset < partitionState.seekOffset) {
                        forward = false;
                        console.log("Skipping", message.topic, message.partition, message.offset);
                    } else {
                        partitionState.seekOffset = null;
                        partitionState.seeking = false;

                    }
                if (forward) {
                    topicState.assignmentStates[message.topic + "-" + message.partition].seekOffset = null;
                    const item = {
                        kafkaHeader: {
                            key: message.key.toString(),
                            offset: message.offset,
                            partition: message.partition,
                            size: message.size,
                            publishTimestamp: new Date(message.timestamp),
                            recieveTimestamp: now,
                            topic: message.topic,
                        },
                        message: message.value
                    }
                    const partitionKey = message.topic + "-" + message.partition;
                    if (!topicMessages[partitionKey]) topicMessages[partitionKey] = [];
                    topicMessages[partitionKey].push(item);
                }
                await this.#eak.yieldEventLoop();
            });
            await async.forEachLimit(Object.keys(topicMessages), 1, async (key) => {
                const topic = topicMessages[key][0].kafkaHeader.topic;
                const partition = topicMessages[key][0].kafkaHeader.partition;
                const batch = {
                    topic: topic,
                    partitionKey: topic + "-" + partition,

                    messages: topicMessages[key]
                }
                batches.push(batch);
                await this.#eak.yieldEventLoop();
            });
        }
        return batches;
    }
}

module.exports = NodeRdKafkaConsumer;