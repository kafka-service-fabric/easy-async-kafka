'use strict';
const { Topic, ConnectionCoordinator } = require("../index");

async function main() {
    const coordinator = new ConnectionCoordinator(["10.10.0.50:9093"], {
        sessionTimeoutMs: 6000,
        groupId: "test",
        batchSize: 200,
        consumerIdleDisconnectMs: 0,
        producerIdleDisconnectMs: 0,
        adminClientIdleDisconnectMs: 0
    });
    const testTopic = new Topic("test", null, {
        consumeMode: 1, //TO_LAST
        //loWater: 500,
        highWater: 1000,
        cleanupPolicy: "compact",
        partitions: 1,
        replicas: 3,
        //autoCommit: true,
        //commitIntervalMessages: 555,
        //commitIntervalMs: 1000,
        deleteRetentionMs: 10000,
        minCleanableDirtyRatio: 0.1,
        segmentMs: 10000,
        segmentBytes: 100000,
        minCompactionLagMs: 10000,
        //maxCompactionLagMs:10000,
        autoUpdateTopicConfig: true,
        maxCompactionLagMs: 70000,
        autoCreateTopic: true,
    }, {
        sessionTimeoutMs: 6000,
        groupId: "test",
        batchSize: 500,
        producerIdleDisconnectMs: 0,
        consumerIdleDisconnectMs: 0,
        adminClientIdleDisconnectMs: 0
    }, coordinator);

    /*const testTopic2 = new Topic("test2", "10.10.0.50:9092", {
        consumeMode: 5, //TO_LAST
        //loWater: 100,
        highWater: 1000,
        cleanupPolicy: "compact",
        partitions:1,
        replicas:3,
        //autoCommit: true,
        //commitIntervalMessages: 555,
        //commitIntervalMs: 1000,
        deleteRetentionMs:10000,
        minCleanableDirtyRatio:0.1,
        segmentMs:10000,
        segmentBytes:1000000,
        minCompactionLagMs:10000,
        //maxCompactionLagMs:10000,
        autoUpdateTopicConfig: true,
        maxCompactionLagMs: 70000,
        autoCreateTopic: true,
    }, {
        sessionTimeoutMs: 6000,
        groupId: "test",
        batchSize: 500,
        consumerIdleDisconnectMs: 1000,
        producerIdleDisconnectMs: 1000,
        adminClientIdleDisconnectMs: 1000
    },testTopic,testTopic);*/

    let start = new Date();
    let count = 0;
    while (count < 50000) {

        await testTopic.produce(count, {count:count});
        count++;
    }
    let end = new Date();
    console.log("Messages:", count);
    console.log("Time Diff:", ((end - start) / 1000).toFixed(1), "s");
    console.log("Produce speed:", (count / ((end - start) / 1000)).toFixed(0), "msg/s");

    //await coordinator.disconnect();
    console.log("CONSUME");

    start = new Date();
    count = 0;
    await testTopic.consume(async (x) => {
        count++;
    }, true)
    end = new Date();
    console.log("Messages:", count);
    console.log("Time Diff:", ((end - start) / 1000).toFixed(1), "s");
    console.log("Consume speed:", (count / ((end - start) / 1000)).toFixed(0), "msg/s");


    //Disconnect consumer
    //await testTopic.consume();
    await coordinator.disconnect();
    console.log("DONE");
}

main();