/*
EAK settings:
        groupId (overwrites kafkaConfiguration: groupId)
        clientId (overwrites kafkaConfiguration: clientId)
        replication (default: 1)
        partitions (default: 1)
        refuseConnectionOnConfigurationMissmatch (default:false)
        cleanupPolicy (default: delete)
        cleanupCompactRetention
        cleanupDeleteRetention
        continueFromNextOffsetOnReasign (default:true)
        (consumerRecieveBuffer)
        producerMaxInFlight
        autoUpdateTopicConfig
        autoIncreaseTopicPartitions
        autoDecreaseTopicPartitions
        autoCreateTopic
        autoCommit (topic must have groupId set to enable) (overwrites kafkaConfiguration: enableAutoCommit)
        commitIntervalMs (topic must have groupId set to enable)
        commitIntervalMessages (topic must have groupId set to enable)
        autoTuneConsumer (default:false) (overwrites some kafkaConfigurations and can retart consumer)
        autoTuneProducer (default:false) (overwrites some kafkaConfigurations and can retart producer)
        resolveProduceOnAcknowledge (default:false)
        resolveConsumeOnEof (default:false)
        resolveCommitOnAcknowledge (default:false)
        disconnectOnEof (default:false)
        consumeMode (EARLIEST,COMMITED,LATEST,NEXT)

        (kafkaTopicConfig)
            groupId (overriden by groupId)
            clientId (overriden by clientId)
            enableAutoCommit (overriden by autoCommit)

        Topic

        consume(null) stops consumer
        consume(async handler(kafkaHeader,message)) starts consumer or changes handler

        commitOffsets() commits last handled message offset for all partitions (topic must have groupId set to enable)
        commitOffsets(partition) commits last handeled message offset for partition (topic must have groupId set to enable)
        commitOffsets([partitions]) commits last handeled message offset for partition (topic must have groupId set to enable)
        commitOffsets({partition,offset}) commits message offset for partition (topic must have groupId set to enable)
        commitOffsets([{partition,offset}]) commits message offset for partition (topic must have groupId set to enable)
        resetCommits(location) resets groupId offset to EARLIEST or LATEST offset (topic must have groupId set to enable)

        produce({key,message}) produces keyed message (update state)
        produce([{key,message}]) produces keyed messages (update state)

        clearMessages() deletes all messages on topic
        clearMessages(clearRules) deletes all messages on topic matching clearRules (Age, Offset, Timestamp)

        getConfig() get current broker config
        getOffsets() get current offsets for topic partitions
        createOnBroker() creates topic on broker according to config
        updateBrokerTopicConfiguration() updates topic on broker with new config 
        recreateOnBorker() recreates topic with new number of partitions and moves messages to new topic (autocreate must be off on the broker)
        deleteFromBroker() deletes topic on broker

        

        Broker
            kafkaBrokers

        Topic    
            >Broker
                >AdminClient
                >Consumers
                >Producer
        
        listTopics() list topics on broker
        getConfig() get current config used by broker
        
        listGroupIds()
        getGroupIdDetails(groupId)
        createGroupId(groupId)
        deleteGroupId(groupId)

        (kafkaBrokerConfig)

        Connection  
            >Consumer
            >



        Consumer 
        subscribe(topic,handler,consumeMode)
        unsubscribe(topic)
        commit(topic,partition,offset)
        pause(topic)
        resume(topic)

        Producer
        send(topic,key,message,maxInFlight)
        
    (eakHighwater)
    (eakLowater)
    eakConsumeMode
    eakBatchSize
    eakAutoCreateTopic


*/
'use strict';
//const EasyAsyncKafka=require("./EasyAsyncKafka");
const EventEmitter = require('events');
const async = require('async');
const uuid = require('uuid').v4;
//const clone = require('clone');
const merge = require('deepmerge')
const shared = require('./shared');
const ConnectionCoordinator = require('./ConnectionCoordinator');
//const { Producer } = require('node-rdkafka');


class Topic extends EventEmitter {

    log = null;
    topicConfiguration = null;
    connectionConfiguration = null;
    topic = null;
    brokers = null;
    consumedRecords = null;
    connectionCoordinator = null;
    moduleName = null;

    #eak = null;

    #recordHandler = null;

    #adminClient = null;
    #producer = null;
    #consumer = null;


    #consumeQueue = null;
    #isSubscribed = false;
    #paused = null;
    #topicOK = null;
    #partitionStates = null;
    #consumedRecords = 0;
    #commitInterval = null;
    #commitTimeout = null;
    #lastCommitConsumedRecords = 0;
    #lastCommitTimestamp = 0;


    constructor(topic, brokers, topicConfiguration, connectionConfiguration, connectionCoordinator, logger, parentName = null) {
        super();
        if (parentName) this.moduleName = parentName + "/" + "Topic";
        else this.moduleName = "EasyAsyncKafka/Topic";

        if (!logger?.info && logger?.log) logger = logger.log;
        if (!(logger?.error && logger?.warn && logger?.info && logger?.debug)) {
            this.log = console;
            this.log.info("Custom logger does not support log.error, log.warn and/or log.info. Reverting to console");
        }
        else if (logger?.child) this.log = logger.child({ module: this.moduleName, context: topic });
        else this.log = logger;

        this.topic = topic;
        this.brokers = shared.toArray(brokers);


        if (!this.topic) {
            shared.throwError(this.log, "A topic must be spesified to create Topic");
        }
        if (!this.brokers && !connectionCoordinator) {
            shared.throwError(this.log, "Brokers must be spesified to create Topic");
        }



        this.topicConfiguration = topicConfiguration || {};
        this.connectionConfiguration = connectionConfiguration || {};

        if (!this.topicConfiguration.consumeMode) this.topicConfiguration.consumeMode = 1; //EARLIEST
        if (!this.topicConfiguration.autoCreateTopic) this.topicConfiguration.autoCreateTopic = false;
        if (!this.topicConfiguration.refuseConfigurationMissmatch) this.topicConfiguration.refuseConfigurationMissmatch = false;
        if (!this.topicConfiguration.refuseReplicasMissmatch) this.topicConfiguration.refuseReplicasMissmatch = false;
        if (!this.topicConfiguration.refusePartitionsMissmatch) this.topicConfiguration.refusePartitionsMissmatch = false;
        if (!this.topicConfiguration.procucerMaxMessagesInFlight) this.topicConfiguration.procucerMaxMessagesInFlight = 100;
        if (!this.topicConfiguration.autoUpdateTopicConfig) this.topicConfiguration.autoUpdateTopicConfig = false;
        if (!this.topicConfiguration.autoCommit) this.topicConfiguration.autoCommit = false;
        if (!this.topicConfiguration.commitIntervalMs) this.topicConfiguration.commitIntervalMs = 100;
        if (!this.topicConfiguration.commitIntervalMessages) this.topicConfiguration.commitIntervalMessages = 100;
        if (!this.topicConfiguration.disconnectOnEof) this.topicConfiguration.disconnectOnEof = false;
        if (!this.topicConfiguration.highWater) this.topicConfiguration.highWater = 500;
        if (this.topicConfiguration.highWater < 2) {
            this.topicConfiguration.highWater = 2;
            this.log.warn("Confuguration highWater must be greater than 1. Setting highWater to 2: " + this.topic + " > " + this.brokers.toString());
        }
        if (!this.topicConfiguration.loWater) {
            this.topicConfiguration.loWater = Math.round(this.topicConfiguration.highWater / 5);
            if (this.topicConfiguration.loWater < 1) this.topicConfiguration.loWater = 1
        }
        if (this.topicConfiguration.loWater < 1) {
            this.topicConfiguration.loWater = 1;
            this.log.warn("Confuguration loWater must be greater than 0. Setting loWater to 1: " + this.topic + " > " + this.brokers.toString());
        }

        if (!this.connectionConfiguration.groupId) this.connectionConfiguration.groupId = uuid();
        if (!this.connectionConfiguration.clientId) this.connectionConfiguration.clientId = this.connectionConfiguration.groupId;

        let adminName = null;
        let consumerName = null;
        let producerName = null;

        if (!connectionCoordinator && !connectionCoordinator?.connectionCoordinator) this.connectionCoordinator = new ConnectionCoordinator(this.brokers, this.connectionConfiguration, this.log, this.moduleName + "/connectionCoordinator");
        else if (connectionCoordinator?.connectionCoordinator) this.connectionCoordinator = connectionCoordinator.connectionCoordinator;
        else this.connectionCoordinator = connectionCoordinator;

        if (!this.brokers) this.brokers = this.connectionCoordinator.defaultBrokers;


        this.#adminClient = this.connectionCoordinator.getAdminClient(this.brokers, this.connectionConfiguration);
        this.#consumer = this.connectionCoordinator.getConsumer(this.brokers, this.connectionConfiguration);
        this.#producer = this.connectionCoordinator.getProducer(this.brokers, this.connectionConfiguration);

        this.#topicOK = false;
        this.#paused = false;
        this.#partitionStates = {};
    }

    async consume(handler, resolveOnEof = false) {
        //Check that consume is not started
        if (handler) {
            //Check if it is OK to consume topic
            await this.#checkAndCreateTopic();

            //Reset message counter
            this.consumedRecords = 0;

            //Set message handler
            this.#recordHandler = handler;

            //Create consume queue
            this.#consumeQueue = new async.queue(async (item) => {
                await this.#handleQueuedMessage(item);
            }, 1);


            let eofPromise = null;
            if (resolveOnEof) {
                eofPromise = new Promise((resolve, reject) => {
                    this.once("eof", () => resolve());
                })
            };


            //Handle EOF on TO_LAST consume mode by removing handler and stopping consuming.
            this.#consumer.once("eof", async (topic, unsubscribeing) => {
                if (topic === this.topic) {
                    if (unsubscribeing) {
                        //this.#isSubscribed = false;
                        this.log.info("Disconecting from topic due to consumeMode TO_LAST (5)");
                        await this.consume();
                    }

                    this.emit("eof", true);
                }
            });

            this.#consumer.once("disconnected", async () => {
                await this.#disconnectConsumer();
            });



            this.#isSubscribed = true;

            await this.#consumer.subscribe(this.topic, this.topicConfiguration, async (topic, items) => {
                await this.#consumeQueue.push(items);

                if (this.#consumeQueue.length() > this.topicConfiguration.highWater && !this.#paused) {
                    this.#paused = true;
                    await this.#consumer.pause(this.topic);
                    this.log.debug("Paused consuming topic: " + this.topic + " > " + this.brokers.toString());
                }
            });

            this.log.info("Started consuming topic: " + this.topic + " > " + this.brokers.toString());

            if (this.topicConfiguration.autoCommit && this.topicConfiguration.commitIntervalMs) {
                this.#commitInterval = setInterval(() => {
                    this.#commit();
                }, this.topicConfiguration.commitIntervalMs);
            }

            await eofPromise;
        } else {
            await this.#disconnectConsumer();
        }
    }

    async #disconnectConsumer() {
        if (this.#isSubscribed) {
            if (this.#commitInterval) {
                clearInterval(this.#commitInterval);
                this.#commitInterval = null;
            }

            if (this.#consumeQueue.length()) {
                this.log.warn("Draining records from queue: " + this.topic + " > " + this.brokers.toString() + " (" + this.#consumeQueue.length() + " message(s))")
                await this.#consumeQueue.drain();
                this.log.debug("Queue drained");
            }

            if (this.#consumer.isConnected()) {
                const autoCommiting = (this.topicConfiguration.commitIntervalMessages || this.topicConfiguration.commitIntervalMs) && this.topicConfiguration.autoCommit;

                if (autoCommiting)
                    await this.#commit(true);

                if (this.topicConfiguration.consumeMode !== 5) await this.#consumer.unsubscribe(this.topic);
            }
            this.#isSubscribed = false;
            this.#recordHandler = null;

            this.#consumeQueue = null;

            this.emit("disconnected", true);
            this.log.info("Ended consuming topic: " + this.topic + " > " + this.brokers.toString() + " (" + this.consumedRecords + " messages)");
        }
    }

    async commit(awaitConfirmation = false) {
        let commit = false;

        if (this.topicConfiguration.commitIntervalMessages) {
            if (!this.#lastCommitConsumedRecords) this.#lastCommitConsumedRecords = this.consumedRecords;
            if ((this.consumedRecords - this.#lastCommitConsumedRecords) >= this.topicConfiguration.commitIntervalMessages) commit = true;
        }

        else if (!commit && this.topicConfiguration.commitIntervalMs && !this.topicConfiguration.autoCommit) {
            const now = new Date();
            if (!this.#commitTimeout) {
                if (this.#lastCommitTimestamp && (now - this.#lastCommitTimestamp) >= this.topicConfiguration.commitIntervalMs) {
                    commit = true;
                } else {
                    this.#commitTimeout = setTimeout(() => {
                        this.#commitTimeout = null;
                        this.#commit();
                    }, this.topicConfiguration.commitIntervalMs);
                }
            }
        }
        else commit = true;

        if (commit) this.#commit(awaitConfirmation);

    }

    async #commit(awaitConfirmation = false) {
        if (this.#lastCommitConsumedRecords !== this.consumedRecords) {
            this.#lastCommitConsumedRecords = this.consumedRecords;
            this.#lastCommitTimestamp = new Date();
            if (awaitConfirmation) await this.#consumer.commit(this.topic);
            else this.#consumer.commit(this.topic);
        }
    }

    async produce(key = "", message, awaitConfirmation = false) {
        if (!this.#topicOK) await this.#checkAndCreateTopic();

        //!!!!!!Add encoder
        let encodedMessage = null;
        if (typeof message === 'object' && message !== null) encodedMessage = JSON.stringify(message);
        else encodedMessage = message;

        await this.#producer.send({
            topic: this.topic,
            key: key.toString(),
            record: encodedMessage
        }, awaitConfirmation);

    }

    async tombstone(key, awaitConfirmation = false) {
        await this.produce(key, null, awaitConfirmation = false)
    }

    async clear() {

    }

    async #checkAndCreateTopic() {
        if (!this.#topicOK) {
            let topicBrokerConfig = await this.#adminClient.getTopicConfig(this.topic);
            if (!topicBrokerConfig && this.topicConfiguration.autoCreateTopic) {
                if (!await this.#adminClient.createTopic(this.topic, this.topicConfiguration)) {
                    shared.throwError(this.log, "Not able to auto create topic: " + this.brokers.toString() + " > " + this.topic);
                };
                topicBrokerConfig = await this.#adminClient.getTopicConfig(this.topic);
            }

            const brokerPartitions = topicBrokerConfig.partitions;
            delete topicBrokerConfig.partitions;
            if (brokerPartitions !== this.topicConfiguration.partitons && this.topicConfiguration.partitons) {
                if (this.topicConfiguration.refusePartitionMissmatch)
                    shared.throwError(this.log, "Can not connected due to partition mismatch with broker and refusePartitionMissmatch config is set for topic: " + this.brokers.toString() + " > " + this.topic);
                else
                    this.log.warn("Topic partitions config are not is not equal to broker config: " + this.brokers.toString() + " > " + this.topic)
            }

            const brokerReplicas = topicBrokerConfig.replicas;
            delete topicBrokerConfig.replicas;
            if (brokerReplicas !== this.topicConfiguration.replicas && this.topicConfiguration.replicas) {
                if (this.topicConfiguration.refuseReplicasMissmatch)
                    shared.throwError(this.log, "Can not connected due to replicas mismatch with broker and refuseReplicasMissmatch config is set for topic: " + this.brokers.toString() + " > " + this.topic);
                else
                    this.log.warn("Topic replicas config are not is not equal to broker config: " + this.brokers.toString() + " > " + this.topic)
            }



            const topicConfigIsEqual = await this.#compareTopicConfig(this.topicConfiguration, topicBrokerConfig)
            if (!topicConfigIsEqual) {
                if (this.topicConfiguration.refuseConfigurationMissmatch)
                    shared.throwError(this.log, "Can not connected due to configuration mismatch with broker and refuseConfigurationMissmatch config is set for topic: " + this.brokers.toString() + " > " + this.topic);
                else {
                    this.log.warn("Topic configuration is not equal on broker: " + this.brokers.toString() + " > " + this.topic)
                    if (this.topicConfiguration.autoUpdateTopicConfig) {
                        const mergedTopicConfiguration = merge(topicBrokerConfig, this.topicConfiguration);
                        if (!await this.#adminClient.updateTopic(this.topic, mergedTopicConfiguration))
                            shared.throwError(this.log, "Unable to auto update topic configuration on broker for topic: " + this.brokers.toString() + " > " + this.topic);
                        else
                            this.log.info("Topic configuration was auto updated on broker: " + this.brokers.toString() + " > " + this.topic)
                    }
                }
            }
            this.#topicOK = true;
        }
    }

    async #compareTopicConfig(topicConfig, brokerTopicConfig) {
        let result = true;
        Object.keys(topicConfig).forEach((key, value) => {
            const topicParameter = topicConfig[key].toString();
            let serverParamater = brokerTopicConfig[key];
            if (topicParameter && serverParamater && serverParamater !== topicParameter)
                result = false;
        })
        return result;
    }

    async #handleQueuedMessage(item) {
        //Handle consumer events (eof/assigned/reassigned/unassigned/disconnected);
        if (this.#isSubscribed) {

            if (item.record) item.record = item.record.toString();
            //!!!!!!! Implement message decoding
            if (this.#recordHandler)
                await this.#recordHandler(item);

            if (!this.consumedRecords)
                this.log.info("Consumed first message from topic: " + this.brokers.toString() + " > " + this.topic);

            this.consumedRecords++;

            if ((this.topicConfiguration.commitIntervalMessages || this.topicConfiguration.commitIntervalMs) && this.topicConfiguration.autoCommit) await this.commit();

            if (!this.#consumeQueue.length() && this.#paused) {
                await this.#consumer.resume(this.topic);
                this.#paused = false;
                this.log.debug("Resumed consuming topic due to empty queue: " + this.brokers + " > " + this.topic);
            }

            if (this.#consumeQueue.length() < this.topicConfiguration.loWater && this.#paused) {
                await this.#consumer.resume(this.topic);
                this.#paused = false;
                this.log.debug("Resumed consuming topic: " + this.brokers + " > " + this.topic);
            }


        } else
            shared.throwError(this.log, "Recieved when not subscribed. Dropping records : " + this.brokers.toString() + " > " + this.topic);
    }
}

module.exports = Topic;